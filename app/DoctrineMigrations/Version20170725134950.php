<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170725134950 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, build_id INT DEFAULT NULL, create_user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, commit VARCHAR(255) NOT NULL, deploy_quantity INT NOT NULL, created_at DATETIME NOT NULL, deployed_at DATETIME DEFAULT NULL, INDEX IDX_389B78317C13F8B (build_id), INDEX IDX_389B78385564492 (create_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tag ADD CONSTRAINT FK_389B78317C13F8B FOREIGN KEY (build_id) REFERENCES build (id)');
        $this->addSql('ALTER TABLE tag ADD CONSTRAINT FK_389B78385564492 FOREIGN KEY (create_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_permission_has_user_action DROP FOREIGN KEY FK_89AE54A09DD27DD1');
        $this->addSql('ALTER TABLE user_permission_has_user_action ADD CONSTRAINT FK_89AE54A09DD27DD1 FOREIGN KEY (user_action_id) REFERENCES user_action (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE build DROP FOREIGN KEY FK_BDA0F2DB3E030ACD');

        $this->addSql('ALTER TABLE application ADD build_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE application ADD CONSTRAINT FK_A45BDDC117C13F8B FOREIGN KEY (build_id) REFERENCES build (id)');

        $this->addSql("UPDATE application a SET a.build_id = (SELECT b.id FROM build b WHERE b.application_id = a.id LIMIT 1)");

        $this->addSql('DROP INDEX IDX_BDA0F2DB3E030ACD ON build');
        $this->addSql('ALTER TABLE build DROP application_id, DROP name');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A45BDDC117C13F8B ON application (build_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tag');
        $this->addSql('ALTER TABLE application DROP FOREIGN KEY FK_A45BDDC117C13F8B');
        $this->addSql('DROP INDEX UNIQ_A45BDDC117C13F8B ON application');
        $this->addSql('ALTER TABLE application DROP build_id');
        $this->addSql('ALTER TABLE build ADD application_id INT DEFAULT NULL, ADD name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE build ADD CONSTRAINT FK_BDA0F2DB3E030ACD FOREIGN KEY (application_id) REFERENCES application (id)');
        $this->addSql('CREATE INDEX IDX_BDA0F2DB3E030ACD ON build (application_id)');
        $this->addSql('ALTER TABLE user_permission_has_user_action DROP FOREIGN KEY FK_89AE54A09DD27DD1');
        $this->addSql('ALTER TABLE user_permission_has_user_action ADD CONSTRAINT FK_89AE54A09DD27DD1 FOREIGN KEY (user_action_id) REFERENCES user_action (id)');
    }
}
