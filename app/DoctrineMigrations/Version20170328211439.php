<?php

namespace Application\Migrations;

use CI\AppBundle\Entity\Type;
use CI\AppBundle\Entity\User;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170328211439 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    protected function getTypes()
    {
        return [
            Type::CODE_HOOK_DEPLOY_STARTING => 'deploy:starting',
            Type::CODE_HOOK_DEPLOY_STARTED => 'deploy:started',
            Type::CODE_HOOK_DEPLOY_UPDATING => 'deploy:updating',
            Type::CODE_HOOK_DEPLOY_UPDATED => 'deploy:updated',
            Type::CODE_HOOK_DEPLOY_PUBLISHING => 'deploy:publishing',
            Type::CODE_HOOK_DEPLOY_PUBLISHED => 'deploy:published',
            Type::CODE_HOOK_DEPLOY_REVERTING => 'deploy:reverting',
            Type::CODE_HOOK_DEPLOY_REVERTED => 'deploy:reverted',
            Type::CODE_HOOK_DEPLOY_FINISHING => 'deploy:finishing',
            Type::CODE_HOOK_DEPLOY_FINISHING_ROLLBACK => 'deploy:finishing_rollback',
            Type::CODE_HOOK_DEPLOY_FINISHED => 'deploy:finished',
            Type::CODE_HOOK_POSITION_AFTER => 'after',
            Type::CODE_HOOK_POSITION_BEFORE => 'before',
        ];
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

        $em = $this->container->get('doctrine.orm.entity_manager');
        $user = $em->getRepository(User::class)->findOneBy(['username' => 'demo']);

        foreach ($this->getTypes() as $code => $name) {
            $type = new Type();
            $type->setCode($code);
            $type->setName($name);
            $type->setCreatedBy($user);
            $type->setUpdatedBy($user);

            $em->persist($type);
        }

        $em->flush();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        foreach ($this->getTypes() as $code => $name) {
            $this->addSql("DELETE FROM `type` WHERE code = :code", [':code' => $code]);
        }
    }
}
