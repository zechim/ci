<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170719125160 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_permission ADD admin TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE user_permission ADD editable TINYINT(1) NOT NULL');

        $this->addSql("UPDATE `user_permission` SET `admin` = 0, `editable` = 1");
        $this->addSql("UPDATE `user_permission` SET `admin` = 1, `editable` = 0 WHERE `name` = 'ADMIN'");
        $this->addSql("UPDATE `user_permission` SET `admin` = 0, `editable` = 0 WHERE `name` = 'API'");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_permission DROP admin');
        $this->addSql('ALTER TABLE user_permission DROP editable');
    }
}
