<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170717185403 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql("UPDATE `type` SET `name` = 'AWAITING' WHERE `code` = 'BUILD_STATUS_AWAITING'");
        $this->addSql("UPDATE `type` SET `name` = 'BUILT' WHERE `code` = 'BUILD_STATUS_BUILT'");
        $this->addSql("UPDATE `type` SET `name` = 'BUILDING' WHERE `code` = 'BUILD_STATUS_BUILDING'");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
