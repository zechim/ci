<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170714160213 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE build (id INT AUTO_INCREMENT NOT NULL, status_type_id INT DEFAULT NULL, create_user_id INT DEFAULT NULL, update_user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, script LONGTEXT DEFAULT NULL, branch VARCHAR(50) NOT NULL, repo VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_BDA0F2DBCD9CFB16 (status_type_id), INDEX IDX_BDA0F2DB85564492 (create_user_id), INDEX IDX_BDA0F2DBE0DFCA6C (update_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE build ADD CONSTRAINT FK_BDA0F2DBCD9CFB16 FOREIGN KEY (status_type_id) REFERENCES type (id)');
        $this->addSql('ALTER TABLE build ADD CONSTRAINT FK_BDA0F2DB85564492 FOREIGN KEY (create_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE build ADD CONSTRAINT FK_BDA0F2DBE0DFCA6C FOREIGN KEY (update_user_id) REFERENCES user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE build');
    }
}
