<?php

namespace Application\Migrations;

use CI\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170424180107 extends AbstractAppMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("ALTER TABLE menu CHANGE icon icon VARCHAR(60) DEFAULT NULL;");

        $this->createRoute('ci_app', 'user_token', 'index', 'credit-card', 'configuration', true, 31);
        $this->createRoute('ci_app', 'user_token', 'show', null, 'user');
        $this->createRoute('ci_app', 'user_token', 'create', null, 'user');
        $this->createRoute('ci_app', 'user_token', 'edit', null, 'user');
        $this->createRoute('ci_app', 'user_token', 'delete', null, 'user');

        $this->createRoute('ci_app', 'server', 'delete', null, 'server');
        $this->createRoute('ci_app', 'application', 'delete', null, 'application');
        $this->createRoute('ci_app', 'hook', 'delete', null, 'hook');

        $this->createRouteUp();

        $this->addSql("INSERT INTO `user_permission` (`name`) VALUES ('API')");
        $this->addSql("INSERT INTO `user_has_user_permission` VALUES ((SELECT `id` FROM `user` WHERE `username` = 'demo'), (SELECT id FROM `user_permission` WHERE name = 'API'))");

        # server api
        $this->createRoute('ci_api', 'server', 'api_create');
        $this->createRoute('ci_api', 'server', 'api_form_create_explain');
        $this->createRoute('ci_api', 'server', 'api_edit');
        $this->createRoute('ci_api', 'server', 'api_delete');

        # application api
        $this->createRoute('ci_api', 'application', 'api_create');
        $this->createRoute('ci_api', 'application', 'api_form_create_explain');
        $this->createRoute('ci_api', 'application', 'api_edit');
        $this->createRoute('ci_api', 'application', 'api_delete');

        $this->createRouteUp('API');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->deleteRoute('ci_app', 'user_token', 'index');
        $this->deleteRoute('ci_app', 'user_token', 'show');
        $this->deleteRoute('ci_app', 'user_token', 'create');
        $this->deleteRoute('ci_app', 'user_token', 'edit');
        $this->deleteRoute('ci_app', 'user_token', 'delete');
        $this->deleteRoute('ci_app', 'server', 'delete');
        $this->deleteRoute('ci_app', 'application', 'delete');
        $this->deleteRoute('ci_app', 'hook', 'delete');

        $this->addSql("ALTER TABLE menu CHANGE icon icon VARCHAR(30) DEFAULT NULL;");
    }
}
