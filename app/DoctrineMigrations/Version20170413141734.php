<?php

namespace Application\Migrations;

use CI\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170413141734 extends AbstractAppMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        # application
        $this->createRoute('ci_app', 'application', 'index', 'phone', null, true);
        $this->createRoute('ci_app', 'application', 'show', null, 'application');
        $this->createRoute('ci_app', 'application', 'create', null, 'application');
        $this->createRoute('ci_app', 'application', 'edit', null, 'application');
        $this->createRoute('ci_app', 'application', 'deploy', null, 'application');
        $this->createRoute('ci_app', 'application', 'upload', null, 'application');
        $this->createRoute('ci_app', 'application', 'show_upload', null, 'application');

        # server
        $this->createRoute('ci_app', 'server', 'index', 'hdd', null, true);
        $this->createRoute('ci_app', 'server', 'show', null, 'server');
        $this->createRoute('ci_app', 'server', 'create', null, 'server');
        $this->createRoute('ci_app', 'server', 'edit', null, 'server');
        $this->createRoute('ci_app', 'server', 'deploy', null, 'server');
        $this->createRoute('ci_app', 'server', 'upload', null, 'server');
        $this->createRoute('ci_app', 'server', 'show_upload', null, 'application');

        # hook
        $this->createRoute('ci_app', 'hook', 'index', 'paperclip', null, true);
        $this->createRoute('ci_app', 'hook', 'show', null, 'hook');
        $this->createRoute('ci_app', 'hook', 'create', null, 'hook');
        $this->createRoute('ci_app', 'hook', 'edit', null, 'hook');

        # history
        $this->createRoute('ci_app', 'history', 'show', null, null);

        # configuration
        $this->createRoute('ci_app', 'configuration', null, 'cog', null, true);

        # user
        $this->createRoute('ci_app', 'user', 'index', 'user', 'configuration', true);
        $this->createRoute('ci_app', 'user', 'show', null, 'user');
        $this->createRoute('ci_app', 'user', 'create', null, 'user');
        $this->createRoute('ci_app', 'user', 'edit', null, 'user');
        $this->createRoute('ci_app', 'user', 'delete', null, 'user');

        # user_action
        $this->createRoute('ci_app', 'user_action', 'index', 'wrench', 'configuration');

        # user_permission
        $this->createRoute('ci_app', 'user_permission', 'index', 'lock', 'configuration', true);
        $this->createRoute('ci_app', 'user_permission', 'show', null, 'user_permission');
        $this->createRoute('ci_app', 'user_permission', 'create', null, 'user_permission');
        $this->createRoute('ci_app', 'user_permission', 'edit', null, 'user_permission');
        $this->createRoute('ci_app', 'user_permission', 'delete', null, 'user_permission');

        $this->addSql("INSERT INTO `user_permission` (`name`) VALUES ('ADMIN')");
        $this->addSql("INSERT INTO `user_has_user_permission` VALUES ((SELECT `id` FROM `user` WHERE `username` = 'demo'), (SELECT id FROM `user_permission` WHERE name = 'ADMIN'))");

        $this->createRouteUp();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("DELETE FROM `user_has_user_permission`");
        $this->addSql("DELETE FROM `user_permission_has_user_action`");
        $this->addSql("DELETE FROM `user_permission`");

        $this->addSql("UPDATE `menu` SET `parent_menu_id` = NULL");
        $this->addSql("DELETE FROM `menu`");
        $this->addSql("DELETE FROM `user_action`");
        $this->addSql("DELETE FROM `user_permission`");
    }
}
