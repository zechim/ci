<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170717184555 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE build ADD application_id INT DEFAULT NULL, ADD auto_deploy TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE build ADD CONSTRAINT FK_BDA0F2DB3E030ACD FOREIGN KEY (application_id) REFERENCES application (id)');
        $this->addSql('CREATE INDEX IDX_BDA0F2DB3E030ACD ON build (application_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE build DROP FOREIGN KEY FK_BDA0F2DB3E030ACD');
        $this->addSql('DROP INDEX IDX_BDA0F2DB3E030ACD ON build');
        $this->addSql('ALTER TABLE build DROP application_id, DROP auto_deploy');
    }
}
