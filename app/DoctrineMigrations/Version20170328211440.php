<?php

namespace Application\Migrations;

use CI\AppBundle\Entity\Hook;
use CI\AppBundle\Entity\User;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170328211440 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    protected function getHooks()
    {
        return [
            "[SF] Symfony Boostrap" => 'execute "php #{release_path}/vendor/sensio/distribution-bundle/Resources/bin/build_bootstrap.php"',
            "[SF] Doctrine Migration" => 'execute "php #{release_path}/bin/console doctrine:migrations:migrate --env=prod --no-interaction"',
            "[SF] Assetic Dump" => 'execute "php #{release_path}/bin/console assetic:dump --env=prod --no-debug"',
            "[SF] Assets Install" => 'execute "php #{release_path}/bin/console assets:install #{release_path}/web"',
            "[SF] Cache Warm Up" => 'execute "php #{release_path}/bin/console cache:warmup --env=prod"',
            "[SF] Cache Clear" => 'execute "php #{release_path}/bin/console cache:clear --no-warmup --env=prod"',
            "[Composer] Install" => 'execute "cd #{release_path} && composer install"',
            "[Composer] Update" => 'execute "cd #{release_path} && composer update"',
        ];
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $user = $em->getRepository(User::class)->findOneBy(['username' => 'demo']);

        foreach ($this->getHooks() as $name => $code) {
            $hook = new Hook();
            $hook->setCode($code);
            $hook->setName($name);
            $hook->setCreatedBy($user);
            $hook->setUpdatedBy($user);

            $em->persist($hook);
        }

        $em->flush();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        foreach ($this->getHooks() as $name => $code) {
            $this->addSql("DELETE FROM `hook` WHERE code = :code", [':code' => $code]);
        }
    }
}
