<?php

namespace Application\Migrations;

use CI\AppBundle\Entity\Hook;
use CI\AppBundle\Entity\Type;
use CI\AppBundle\Entity\User;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170328211441 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("DELETE server_config FROM server_config JOIN `type` ON server_config.config_type_id = `type`.id AND `type`.code = :code", [':code' => Type::CODE_CONFIG_REPO]);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
