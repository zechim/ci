<?php

namespace Application\Migrations;

use CI\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use CI\AppBundle\Entity\Type;
use CI\AppBundle\Entity\User;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170424180113 extends AbstractAppMigration
{
    protected function getTypes()
    {
        return [
            /* Server.statusType */
            Type::CODE_SERVER_STATUS_RUNNING_HOOK => 'RUNNING HOOK',
            Type::CODE_SERVER_STATUS_HOOK_RAN => 'HOOK RAN',
        ];
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $em = $this->container->get('doctrine.orm.entity_manager');

        $user = $em->getRepository(User::class)->findOneBy(['id' => 1]);

        foreach ($this->getTypes() as $code => $name) {
            $type = new Type();
            $type->setCode($code);
            $type->setName($name);
            $type->setCreatedBy($user);
            $type->setUpdatedBy($user);

            $em->persist($type);
        }

        $em->flush();

        $this->createRoute('ci_api', 'hook', 'api_run');
        $this->createRouteUp('API');

        $this->createRoute('ci_app', 'hook', 'run', null, 'hook');
        $this->createRouteUp();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        foreach ($this->getTypes() as $code => $name) {
            $this->addSql("DELETE FROM `type` WHERE `code` = :code", [':code' => $code]);
        }

        $this->deleteRoute('ci_api', 'hook', 'api_run');
        $this->deleteRoute('ci_app', 'hook', 'run');
    }
}
