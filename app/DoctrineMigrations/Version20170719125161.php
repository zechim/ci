<?php

namespace Application\Migrations;

use CI\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use CI\AppBundle\Entity\Build;
use CI\AppBundle\Entity\UserAction;
use CI\AppBundle\Repository\BuildRepository;
use CI\AppBundle\Repository\UserActionRepository;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170719125161 extends AbstractAppMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        /** @var BuildRepository $buildRepo */
        $buildRepo = $this->container->get('doctrine.orm.entity_manager')->getRepository(Build::class);

        /** @var UserActionRepository $userActionRepo */
        $userActionRepo = $this->container->get('doctrine.orm.entity_manager')->getRepository(UserAction::class);

        foreach ($buildRepo->findAll() as $build) {
            $userActionRepo->createEntityPermission($build);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /** @var BuildRepository $buildRepo */
        $buildRepo = $this->container->get('doctrine.orm.entity_manager')->getRepository(Build::class);

        /** @var UserActionRepository $userActionRepo */
        $userActionRepo = $this->container->get('doctrine.orm.entity_manager')->getRepository(UserAction::class);

        foreach ($buildRepo->findAll() as $build) {
            $userActionRepo->removeEntityPermission($build);
        }
    }
}
