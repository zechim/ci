<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170413140425 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_permission (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, UNIQUE INDEX uq_name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_permission_has_user_action (user_permission_id INT NOT NULL, user_action_id INT NOT NULL, INDEX IDX_89AE54A01057A19A (user_permission_id), INDEX IDX_89AE54A09DD27DD1 (user_action_id), PRIMARY KEY(user_permission_id, user_action_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE menu (id INT AUTO_INCREMENT NOT NULL, parent_menu_id INT DEFAULT NULL, user_action_id INT DEFAULT NULL, page_name VARCHAR(60) DEFAULT NULL, menu_name VARCHAR(60) DEFAULT NULL, icon VARCHAR(30) DEFAULT NULL, route VARCHAR(200) DEFAULT NULL, position INT DEFAULT NULL, is_visible INT NOT NULL, INDEX IDX_7D053A93BE9F9D54 (parent_menu_id), INDEX IDX_7D053A939DD27DD1 (user_action_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_has_user_permission (user_id INT NOT NULL, user_permission_id INT NOT NULL, INDEX IDX_7B1F321DA76ED395 (user_id), INDEX IDX_7B1F321D1057A19A (user_permission_id), PRIMARY KEY(user_id, user_permission_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_action (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(60) NOT NULL, code VARCHAR(50) NOT NULL, UNIQUE INDEX uq_code (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_permission_has_user_action ADD CONSTRAINT FK_89AE54A01057A19A FOREIGN KEY (user_permission_id) REFERENCES user_permission (id)');
        $this->addSql('ALTER TABLE user_permission_has_user_action ADD CONSTRAINT FK_89AE54A09DD27DD1 FOREIGN KEY (user_action_id) REFERENCES user_action (id)');
        $this->addSql('ALTER TABLE menu ADD CONSTRAINT FK_7D053A93BE9F9D54 FOREIGN KEY (parent_menu_id) REFERENCES menu (id)');
        $this->addSql('ALTER TABLE menu ADD CONSTRAINT FK_7D053A939DD27DD1 FOREIGN KEY (user_action_id) REFERENCES user_action (id)');
        $this->addSql('ALTER TABLE user_has_user_permission ADD CONSTRAINT FK_7B1F321DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_has_user_permission ADD CONSTRAINT FK_7B1F321D1057A19A FOREIGN KEY (user_permission_id) REFERENCES user_permission (id)');
        $this->addSql('ALTER TABLE user ADD token VARCHAR(60) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_permission_has_user_action DROP FOREIGN KEY FK_89AE54A01057A19A');
        $this->addSql('ALTER TABLE user_has_user_permission DROP FOREIGN KEY FK_7B1F321D1057A19A');
        $this->addSql('ALTER TABLE menu DROP FOREIGN KEY FK_7D053A93BE9F9D54');
        $this->addSql('ALTER TABLE user_permission_has_user_action DROP FOREIGN KEY FK_89AE54A09DD27DD1');
        $this->addSql('ALTER TABLE menu DROP FOREIGN KEY FK_7D053A939DD27DD1');
        $this->addSql('DROP TABLE user_permission');
        $this->addSql('DROP TABLE user_permission_has_user_action');
        $this->addSql('DROP TABLE menu');
        $this->addSql('DROP TABLE user_has_user_permission');
        $this->addSql('DROP TABLE user_action');
        $this->addSql('ALTER TABLE user DROP token');
    }
}
