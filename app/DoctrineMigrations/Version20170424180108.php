<?php

namespace Application\Migrations;

use CI\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170424180108 extends AbstractAppMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        # hook api
        $this->createRoute('ci_api', 'hook', 'api_index');
        $this->deleteRoute('ci_api', 'server', 'api_form_create_explain');
        $this->deleteRoute('ci_api', 'application', 'api_form_create_explain');

        $this->createRoute('ci_api', 'server', 'api_form_explain');
        $this->createRoute('ci_api', 'application', 'api_form_explain');

        $this->createRouteUp('API');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->deleteRoute('ci_api', 'hook', 'api_index');
        $this->deleteRoute('ci_api', 'server', 'api_form_explain');
        $this->deleteRoute('ci_api', 'application', 'api_form_explain');
    }
}
