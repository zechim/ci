<?php

namespace Application\Migrations;

use CI\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use CI\AppBundle\Entity\Type;
use CI\AppBundle\Entity\User;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170713123506 extends AbstractAppMigration
{
    protected function getTypes()
    {
        return [
            /* Build.statusType */
            Type::CODE_BUILD_STATUS_BUILT => 'Gerado',
            Type::CODE_BUILD_STATUS_BUILDING => 'Gerando',
        ];
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

        # configuration
        $this->createRoute('ci_app', 'tool', null, 'wrench', null, true, 21);

        $this->createRoute('ci_app', 'build', 'index', 'plane', 'tool', true, 1);
        $this->createRoute('ci_app', 'build', 'show', null);
        $this->createRoute('ci_app', 'build', 'create', null);
        $this->createRoute('ci_app', 'build', 'edit', null);
        $this->createRoute('ci_app', 'build', 'delete', null);
        $this->createRoute('ci_app', 'build', 'build', null);
        $this->createRoute('ci_app', 'build', 'show_upload', null);

        $this->createRouteUp();

        $em = $this->container->get('doctrine.orm.entity_manager');
        $user = $em->getRepository(User::class)->findOneBy(['username' => 'demo']);

        foreach ($this->getTypes() as $code => $name) {
            $type = new Type();
            $type->setCode($code);
            $type->setName($name);
            $type->setCreatedBy($user);
            $type->setUpdatedBy($user);

            $em->persist($type);
        }

        $em->flush();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

        $this->deleteRoute('ci_app', 'build', 'index');
        $this->deleteRoute('ci_app', 'build', 'show');
        $this->deleteRoute('ci_app', 'build', 'create');
        $this->deleteRoute('ci_app', 'build', 'edit');
        $this->deleteRoute('ci_app', 'build', 'delete');

        $this->addSql("DELETE FROM `menu` WHERE `page_name` = 'trans.menu.tool'");

        foreach ($this->getTypes() as $code => $name) {
            $this->addSql("DELETE FROM `type` WHERE code = :code", [':code' => $code]);
        }
    }
}
