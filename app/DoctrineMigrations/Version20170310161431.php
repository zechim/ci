<?php

namespace Application\Migrations;

use CI\AppBundle\Entity\Type;
use CI\AppBundle\Entity\User;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170310161431 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    protected function getTypes()
    {
        return [
            Type::CODE_CONFIG_BRANCH => 'Branch',
            Type::CODE_CONFIG_KEY => 'Private Key',
            Type::CODE_CONFIG_REPO => 'Repo',
            Type::CODE_CONFIG_DEPLOY_TO => 'Deploy Path',
            Type::CODE_CONFIG_IP => 'IP',
            Type::CODE_CONFIG_USER => 'User',

            /* Server.statusType */
            Type::CODE_SERVER_STATUS_AWAITING_PROCESSING => 'AWAITING PROCESSING',
            Type::CODE_SERVER_STATUS_DUMPING => 'DUMPING',
            Type::CODE_SERVER_STATUS_DUMPED => 'DUMPED',
            Type::CODE_SERVER_STATUS_DEPLOYING => 'DEPLOYING',
            Type::CODE_SERVER_STATUS_DEPLOYED => 'DEPLOYED',
            Type::CODE_SERVER_STATUS_READY => 'READY',

            /* Application.statusType */
            Type::CODE_APPLICATION_STATUS_AWAITING_PROCESSING => 'AWAITING PROCESSING',
            Type::CODE_APPLICATION_STATUS_READY => 'READY',

            /* Queue.statusType */
            Type::CODE_QUEUE_STATUS_WAITING => 'WAITING',
            Type::CODE_QUEUE_STATUS_RUNNING => 'RUNNING',
            Type::CODE_QUEUE_STATUS_EXECUTED => 'EXECUTED',
            Type::CODE_QUEUE_STATUS_ERROR => 'ERROR',
        ];
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $user = new User();
        $user->setActive(true);
        $user->setName('demo');
        $user->setUsername('demo');

        $user->setPassword($this->container->get('security.password_encoder')->encodePassword($user, 'demo'));

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($user);
        $em->flush($user);

        $user->setUpdatedBy($user);
        $user->setCreatedBy($user);

        $em->persist($user);
        $em->flush($user);

        foreach ($this->getTypes() as $code => $name) {
            $type = new Type();
            $type->setCode($code);
            $type->setName($name);
            $type->setCreatedBy($user);
            $type->setUpdatedBy($user);

            $em->persist($type);
        }

        $em->flush();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("UPDATE `user` SET `create_user_id`= NULL, `update_user_id`= NULL");
        $this->addSql("DELETE FROM `type`");
        $this->addSql("DELETE FROM `user`");
    }
}
