<?php

namespace Application\Migrations;

use CI\AppBundle\Doctrine\DBAL\Migrations\AbstractAppMigration;
use CI\AppBundle\Entity\Type;
use CI\AppBundle\Entity\User;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\QueryBuilder;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170728193506 extends AbstractAppMigration
{
    protected function getTypes()
    {
        return [
            /* Tag.statusType */
            Type::CODE_TAG_STATUS_READY => 'READY',
            Type::CODE_TAG_STATUS_DELETING => 'DELETING',
        ];
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tag ADD status_type_id INT DEFAULT NULL, CHANGE deploy_quantity build_quantity INT NOT NULL, CHANGE deployed_at built_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE tag ADD CONSTRAINT FK_389B783CD9CFB16 FOREIGN KEY (status_type_id) REFERENCES type (id)');
        $this->addSql('CREATE INDEX IDX_389B783CD9CFB16 ON tag (status_type_id)');

        $em = $this->container->get('doctrine.orm.entity_manager');

        /** @var QueryBuilder $qb */
        $qb = $em->getRepository(User::class)->createQueryBuilder('u');
        $qb->setMaxResults(1);
        $qb->orderBy('u.id', 'ASC');

        $user = $qb->getQuery()->getSingleResult();

        foreach ($this->getTypes() as $code => $name) {
            $type = new Type();
            $type->setCode($code);
            $type->setName($name);
            $type->setCreatedBy($user);
            $type->setUpdatedBy($user);

            $em->persist($type);
        }

        $em->flush();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tag DROP FOREIGN KEY FK_389B783CD9CFB16');
        $this->addSql('DROP INDEX IDX_389B783CD9CFB16 ON tag');
        $this->addSql('ALTER TABLE tag DROP status_type_id, CHANGE build_quantity deploy_quantity INT NOT NULL, CHANGE built_at deployed_at DATETIME DEFAULT NULL');

        foreach ($this->getTypes() as $code => $name) {
            $this->addSql("DELETE FROM `type` WHERE code = :code", [':code' => $code]);
        }
    }
}
