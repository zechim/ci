<?php

namespace CI\AppBundle\EventListener;

use CI\AppBundle\Service\Menu\MenuRequestFetcher;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class MenuRequestFetcherListener
{
    /**
     * @var \CI\AppBundle\Service\Menu\MenuRequestFetcher
     */
    protected $menuRequestFetcher;
        
    public function __construct(MenuRequestFetcher $menuRequestFetcher)
    {
        $this->menuRequestFetcher = $menuRequestFetcher;
    }
    
    public function onKernelController(FilterControllerEvent $event)
    {
        $this->menuRequestFetcher->fetch($event->getRequest());
    }
}
