<?php

namespace CI\AppBundle\EventListener;
 

use CI\AppBundle\Exception\TransException;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Translation\TranslatorInterface;

class ControllerExceptionListener
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\Templating\EngineInterface
     */
    protected $templating;
    
    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    protected $translator;
    
    /**
     * @var string
     */
    protected $env;
    
    public function __construct(EngineInterface $templating, TranslatorInterface $translator, $env)
    {
        $this->templating = $templating;
        $this->translator = $translator;
        $this->env = $env;
    }
    
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ('dev' === $this->env && false === $event->getException() instanceof TransException) {
            return;
        }

        if (true === $event->getException() instanceof TransException) {
            $message = $event->getException()->trans($this->translator);

        } else {
            $message = $event->getException()->getMessage();

        }
        
        $response = new Response();
        $response->setContent(
            $this->templating->render(
                'CIAppBundle:Default:error.html.twig',
                ['message' => $message]
            )
        );
        
        $event->setResponse($response);
    }
}
