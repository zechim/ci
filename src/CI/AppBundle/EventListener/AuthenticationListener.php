<?php

namespace CI\AppBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class AuthenticationListener
{
    const TOKEN_SESSION_KEY = 'token';
    
    /**
     * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
     */
    protected $session;
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __construct(SessionInterface $session, EntityManager $em)
    {
        $this->session = $session;
        $this->em = $em;
    }
    
    public function onAuthenticationSuccess(InteractiveLoginEvent $event)
    {
        $token = uniqid(null, true);
        $user = $event->getAuthenticationToken()->getUser();
        
        $this->session->set(self::TOKEN_SESSION_KEY, $token);
        
        $user->setToken($token);
        $user->setUpdatedAt(new \DateTime());
        
        $this->em->persist($user);
        $this->em->flush($user);
    }
}
