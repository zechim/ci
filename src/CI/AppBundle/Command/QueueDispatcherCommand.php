<?php

namespace CI\AppBundle\Command;

use CI\AppBundle\Entity\Queue;
use CI\AppBundle\Repository\QueueRepository;
use CI\AppBundle\Service\Queue\QueueDispatcher;
use CI\AppBundle\Service\Queue\QueueErrorRecovery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

register_shutdown_function(function(){
    return QueueErrorRecovery::shutDown();
});

class QueueDispatcherCommand extends ContainerAwareCommand
{
    const OPTION_QUEUE = 'q';
    
    protected function configure()
    {
        parent::configure();

        $this->setName('ci:queue')->addOption(self::OPTION_QUEUE, null, InputOption::VALUE_REQUIRED, 'Queue');
    }

    /**
     * @return bool
     */
    protected function isRunning()
    {
        $cmd = explode("\n", shell_exec("ps p " . getmypid() . '  o cmd'));
        return count(explode("\n", shell_exec('ps -ef | grep -E "' . trim(str_replace("\\", "\\\\\\\\", $cmd[1])) . '"'))) >= 5;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        QueueErrorRecovery::$doctrine = $this->getContainer()->get('doctrine');
        QueueErrorRecovery::$output = $output;

        if (true === $this->isRunning()) {
            $output->writeln(sprintf("Queue [%s] already running", $input->getOption(self::OPTION_QUEUE)));
            return;
        }

        /** @var QueueRepository $queueRepo */
        $queueRepo = $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository(Queue::class);

        /** @var QueueDispatcher $queueDispatcher */
        $queueDispatcher = $this->getContainer()->get('ci.queue.dispatcher');

        $queue = $queueRepo->findOneWaitingById($input->getOption(self::OPTION_QUEUE));

        if (null === $queue) {
            $output->writeln("Nothing found");

            return;
        }

        QueueErrorRecovery::$queue = $queue;

        try {
            $output->writeln("Running Queue [{$queue->getId()}] with Service [{$queue->getService()}] at " . date('Y-m-d H:i:s') . ".");

            $queueDispatcher->dispatch($queue);

            QueueErrorRecovery::$queue = null;

        } catch (\Exception $e) {
            QueueErrorRecovery::setError($e->getFile(), $e->getLine(), $e->getMessage());
            return;
        }
    }
}

