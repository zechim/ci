<?php

namespace CI\AppBundle\Command;

use CI\AppBundle\Entity\Server;
use CI\AppBundle\Repository\ServerRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DeployCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('ci:deploy')
            ->addOption('server', 's', InputOption::VALUE_REQUIRED, 'Server ID')
            ->addArgument('deploy', InputArgument::OPTIONAL, 'Deploy', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $server = (int) $input->getOption('server');

        if (0 >= $server) {
            return 0;
        }

        /** @var ServerRepository $serverRepo */
        $serverRepo = $this->getContainer()->get('doctrine.orm.entity_manager')->getRepository(Server::class);

        /** @var Server $server */
        $server = $serverRepo->find($server);

        if (null === $server) {
            return 0;
        }

        $serverRepo->dump($server, $server->getCreatedBy(), false !== $input->getArgument('deploy'));

        return 1;
    }
}
