<?php

namespace CI\AppBundle\Doctrine\DBAL\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
abstract class AbstractAppMigration extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $routes;

    /**
     * @var array
     */
    protected $actions;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param string $prefix
     * @param $name
     * @param null $action
     * @param null $icon
     * @param null $parent
     * @param bool $visible
     * @param null $position
     */
    protected function createRoute($prefix = 'ci_app', $name, $action = null, $icon = null, $parent = null, $visible = false, $position = null)
    {
        $route = [
            'page_name' => sprintf("'trans.menu.%s'", $name),
            'menu_name' => sprintf("'trans.menu.%s'", $name),
            'position' => null === $position ? count($this->routes) : $position,
            'is_visible' => (int) $visible
        ];

        if (null !== $action) {
            $route['route'] = sprintf("'%s_%s_%s'", $prefix, $name, $action);
            $route['user_action_id'] = sprintf("(SELECT id FROM user_action WHERE code = '%s_%s')", strtoupper($name), strtoupper($action));

            $this->actions[] = [
                'name' => sprintf('%s (%s)', strtoupper($name), sprintf('%s_%s', strtoupper($name), strtoupper($action))),
                'code' => sprintf('%s_%s', strtoupper($name), strtoupper($action)),
            ];
        }

        if (null !== $icon) {
            $route['icon'] = sprintf("'glyphicon glyphicon-%s'", $icon);
        }

        if (null !== $parent) {
            $route['parent_menu_id'] = sprintf("(SELECT m.id FROM menu m WHERE m.page_name = 'trans.menu.%s' ORDER BY 1 ASC LIMIT 1)", $parent);
        }

        $this->routes[] = $route;
    }

    /**
     * @param string $permission
     */
    protected function createRouteUp($permission = 'ADMIN')
    {
        # actions
        foreach ($this->actions as $action) {
            $this->addSql(
                sprintf(
                    "INSERT INTO `user_action` (`%s`) VALUES ('%s')",
                    implode('`, `', array_keys($action)),
                    implode("', '", $action)
                )
            );
        };

        # routes
        foreach ($this->routes as $route) {
            $this->addSql(
                sprintf(
                    "INSERT INTO `menu` (`%s`) VALUES (%s)",
                    implode('`, `', array_keys($route)),
                    implode(", ", $route)
                )
            );
        };

        if (0 === count($this->routes)) {
            return;
        }

        $this->addSql("INSERT INTO `user_permission_has_user_action` SELECT (SELECT id FROM `user_permission` WHERE name = :permission), ua.id FROM `user_action` ua WHERE NOT EXISTS (SELECT user_action_id FROM `user_permission_has_user_action` uphua WHERE uphua.user_action_id = ua.id)", [':permission' => $permission]);

        $this->routes = [];
        $this->actions = [];
    }

    /**
     * @param string $prefix
     * @param $name
     * @param $action
     */
    public function deleteRoute($prefix = 'ci_app', $name, $action)
    {
        $route = sprintf('%s_%s_%s', $prefix, $name, $action);
        $code = sprintf('%s_%s', strtoupper($name), strtoupper($action));

        $this->addSql('DELETE FROM `user_permission_has_user_action` WHERE user_action_id = (SELECT user_action_id FROM `menu` WHERE route = :route)', [':route' => $route]);
        $this->addSql("UPDATE `menu` SET `parent_menu_id` = NULL WHERE route = :route", [':route' => $route]);
        $this->addSql("DELETE FROM `menu` WHERE route = :route", [':route' => $route]);
        $this->addSql('DELETE FROM `user_action` WHERE code = :code', [':code' => $code]);
    }
}
