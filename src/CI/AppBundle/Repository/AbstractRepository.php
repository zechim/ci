<?php

namespace CI\AppBundle\Repository;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\ApplicationConfig;
use CI\AppBundle\Entity\Build;
use CI\AppBundle\Entity\History;
use CI\AppBundle\Entity\Queue;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\Tag;
use CI\AppBundle\Entity\Type;
use CI\AppBundle\Entity\UserAction;
use CI\AppBundle\Entity\UserPermission;
use Doctrine\ORM\EntityRepository;

abstract class AbstractRepository extends EntityRepository
{
    /**
     * @param $code
     * @return Type|object
     */
    public function getTypeByCode($code)
    {
        return $this->getTypeRepository()->findOneBy(['code' => $code]);
    }

    /**
     * @return TypeRepository|EntityRepository
     */
    public function getTypeRepository()
    {
        return $this->_em->getRepository(Type::class);
    }

    /**
     * @return QueueRepository|EntityRepository
     */
    public function getQueueRepository()
    {
        return $this->_em->getRepository(Queue::class);
    }

    /**
     * @return HistoryRepository|EntityRepository
     */
    public function getHistoryRepository()
    {
        return $this->_em->getRepository(History::class);
    }

    /**
     * @return ServerRepository|EntityRepository
     */
    public function getServerRepository()
    {
        return $this->_em->getRepository(Server::class);
    }

    /**
     * @return ApplicationRepository|EntityRepository
     */
    public function getApplicationRepository()
    {
        return $this->_em->getRepository(Application::class);
    }

    /**
     * @return ApplicationConfigRepository|EntityRepository
     */
    public function getApplicationConfigRepository()
    {
        return $this->_em->getRepository(ApplicationConfig::class);
    }

    /**
     * @return UserActionRepository|EntityRepository
     */
    public function getUserActionRepository()
    {
        return $this->_em->getRepository(UserAction::class);
    }

    /**
     * @return UserPermissionRepository|EntityRepository
     */
    public function getUserPermissionRepository()
    {
        return $this->_em->getRepository(UserPermission::class);
    }

    /**
     * @return BuildRepository|EntityRepository
     */
    public function getBuildRepository()
    {
        return $this->_em->getRepository(Build::class);
    }

    /**
     * @return TagRepository|EntityRepository
     */
    public function getTagRepository()
    {
        return $this->_em->getRepository(Tag::class);
    }
}
