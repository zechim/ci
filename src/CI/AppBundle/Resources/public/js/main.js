requirejs.config({
    baseUrl: window.basePath,
    urlArgs: "t=" + (new Date()).getTime(),
    paths: {
        'jquery': 'bundles/ciapp/js/vendor/jquery-3.1.1.min',
        'bootstrap': 'bundles/ciapp/js/vendor/bootstrap.min',
        'underscore': 'bundles/ciapp/js/vendor/underscore-min',
        'jquery.hotkeys': 'bundles/ciapp/js/vendor/jquery.hotkeys',
    },
    shim: {
        'underscore': {exports: '_'},
        'jquery.hotkeys' : {deps: ['jquery']},
        'bootstrap' : {deps: ['jquery']},
        'select2' : {deps: ['jquery']},
    }
});