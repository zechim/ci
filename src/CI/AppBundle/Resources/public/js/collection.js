function Collection() {
    this.holder = null;
    this.childTemplate = null;
    this.choiceBag = [];
    this.choiceBagIndex = -1;
};

Collection.prototype = {
    constructor: Collection
};

Collection.prototype.load = function() {
    var that = this;

    this.holder.find('> tr').each(function(i) {
        var index = $(this).find('td:eq(0) > input, td:eq(0) > select').attr('id').replace(/[^\d.]/g, '');
        that.choiceBagIndex = index > that.choiceBagIndex ? index : that.choiceBagIndex;

        var item = new Child(that);
        item.setIndex(index);

        that.addChoice(item, i);
    });
};

Collection.prototype.createEmptyChild = function() {
    this.choiceBagIndex = this.choiceBagIndex + 1;

    var template = this.childTemplate.replace(/__name__/g, this.choiceBagIndex);

    this.holder.append(template);

    var referenceIndex = this.holder.children().length - 1;

    var item = new Child(this);
    item.setIndex(this.choiceBagIndex);

    this.addChoice(item, referenceIndex);
};

Collection.prototype.removeItem = function(item) {
    item.getReference().remove();

    delete this.choiceBag[item.getIndex()];
};

Collection.prototype.addChoice = function(item, referenceIndex) {
    item.bind(referenceIndex);

    this.choiceBag[item.getIndex()] = item;
};

function Child(field) {
    this.field = field;
    this.index = null;
    this.reference = null;
};

Child.prototype = {
    constructor: Child,

    setIndex: function(index) {
        this.index = index;
    },

    getIndex: function() {
        return this.index;
    },

    getReference: function() {
        return this.reference;
    }
};

Child.prototype.bind = function(referenceIndex) {
    this.reference = this.field.holder.find("> tr:eq(" + referenceIndex + ")");

    var that = this;

    // remove button
    this.reference.find("> td:eq(-1) a").click(function() {
        that.field.removeItem(that);
    });

    var max = 0;

    this.reference.parent().find('tr').each(function() {
        var v = parseInt($(this).find('> td.position input').val()) || 0;
        max = v > max ? v : max;

    }).promise().done(function() {
        var v = parseInt(that.reference.find('> td.position input').val()) || 0;

        if (v <= 0) {
            that.reference.find("> td.position input").val(max + 1);
        }
    });
}