<?php

namespace CI\AppBundle\Twig;

use CI\AppBundle\Entity\Hook;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\Type;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ServerExtension extends \Twig_Extension
{
    /**
     * @var \CI\AppBundle\Repository\ServerRepository
     */
    protected $serverRepository;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorization;

    public function __construct(EntityManagerInterface $em, AuthorizationCheckerInterface $authorization)
    {
        $this->serverRepository = $em->getRepository(Server::class);
        $this->authorization = $authorization;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('SERVER_status_label', [$this, 'renderStatusLabel'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('SERVER_can_deploy', [$this, 'canDeploy']),
            new \Twig_SimpleFunction('SERVER_can_edit', [$this, 'canEdit']),
            new \Twig_SimpleFunction('SERVER_can_run_hook', [$this, 'canRunHook'])
        );
    }

    /**
     * @param Server $server
     * @return string
     */
    public function renderStatusLabel(Server $server)
    {
        $types = [
            Type::CODE_SERVER_STATUS_AWAITING_PROCESSING => 'info',
            Type::CODE_SERVER_STATUS_DUMPED => 'info',
            Type::CODE_SERVER_STATUS_DEPLOYED => 'info',
            Type::CODE_SERVER_STATUS_HOOK_RAN => 'info',
            Type::CODE_SERVER_STATUS_READY => 'success',
            Type::CODE_SERVER_STATUS_DUMPING => 'warning',
            Type::CODE_SERVER_STATUS_RUNNING_HOOK => 'warning',
            Type::CODE_SERVER_STATUS_DEPLOYING => 'primary',
        ];

        return sprintf(
            '<span class="label label-%s">%s</span>',
            $types[$server->getStatusType()->getCode()],
            $server->getStatusType()
        );
    }

    /**
     * @param Server $server
     * @return bool
     */
    public function canDeploy(Server $server)
    {
        return $this->authorization->isGranted('ROLE_SERVER_DEPLOY') && $this->serverRepository->canDeploy($server);
    }

    /**
     * @param Server $server
     * @return bool
     */
    public function canEdit(Server $server)
    {
        return $this->authorization->isGranted('ROLE_SERVER_EDIT') && $this->serverRepository->canEdit($server);
    }

    /**
     * @param Server $server
     * @param Hook $hook
     * @return bool
     */
    public function canRunHook(Server $server, Hook $hook)
    {
        return $this->authorization->isGranted('ROLE_HOOK_RUN') && $this->serverRepository->canRunHook($server, $hook);
    }

    public function getName()
    {
        return 'ci_app_server_extension';
    }

}