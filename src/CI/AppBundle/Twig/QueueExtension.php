<?php

namespace CI\AppBundle\Twig;

use CI\AppBundle\Entity\Queue;
use CI\AppBundle\Entity\Type;

class QueueExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('QUEUE_status_label', [$this, 'renderStatusLabel'], ['is_safe' => ['html']]),
        );
    }

    /**
     * @param Queue $queue
     * @return string
     */
    public function renderStatusLabel(Queue $queue)
    {
        $types = [
            Type::CODE_QUEUE_STATUS_WAITING => 'primary',
            Type::CODE_QUEUE_STATUS_EXECUTED => 'success',
            Type::CODE_QUEUE_STATUS_RUNNING => 'warning',
            Type::CODE_QUEUE_STATUS_ERROR => 'danger',
        ];

        return sprintf(
            '<span class="label label-%s">%s</span>',
            $types[$queue->getStatusType()->getCode()],
            $queue->getStatusType()
        );
    }

    public function getName()
    {
        return 'ci_app_queue_extension';
    }

}