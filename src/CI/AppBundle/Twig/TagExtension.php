<?php

namespace CI\AppBundle\Twig;

use CI\AppBundle\Entity\Tag;
use CI\AppBundle\Entity\Type;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class TagExtension extends \Twig_Extension
{
    /**
     * @var \CI\AppBundle\Repository\TagRepository
     */
    protected $tagRepository;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorization;

    public function __construct(EntityManagerInterface $em, AuthorizationCheckerInterface $authorization)
    {
        $this->tagRepository = $em->getRepository(Tag::class);
        $this->authorization = $authorization;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('TAG_status_label', [$this, 'renderStatusLabel'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('TAG_can_build', [$this, 'canBuild']),
            new \Twig_SimpleFunction('TAG_can_delete', [$this, 'canDelete']),
        );
    }

    /**
     * @param Tag $tag
     * @return string
     */
    public function renderStatusLabel(Tag $tag)
    {
        $types = [
            Type::CODE_TAG_STATUS_DELETING => 'info',
            Type::CODE_TAG_STATUS_READY => 'success',
        ];

        return sprintf(
            '<span class="label label-%s">%s</span>',
            $types[$tag->getStatusType()->getCode()],
            $tag->getStatusType()
        );
    }

    /**
     * @param Tag $tag
     * @return bool
     */
    public function canBuild(Tag $tag)
    {
        return
            $this->authorization->isGranted('ROLE_TAG_BUILD') &&
            $this->authorization->isGranted('ROLE_BUILD_BUILD') &&
            $this->tagRepository->canBuild($tag);
    }

    /**
     * @param Tag $tag
     * @return bool
     */
    public function canDelete(Tag $tag)
    {
        return $this->authorization->isGranted('ROLE_TAG_DELETE') && $this->tagRepository->canDelete($tag);
    }

    public function getName()
    {
        return 'ci_app_tag_extension';
    }

}