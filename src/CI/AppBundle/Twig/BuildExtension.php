<?php

namespace CI\AppBundle\Twig;

use CI\AppBundle\Entity\Build;
use CI\AppBundle\Entity\Type;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class BuildExtension extends \Twig_Extension
{
    /**
     * @var \CI\AppBundle\Repository\BuildRepository
     */
    protected $buildRepository;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorization;

    public function __construct(EntityManagerInterface $em, AuthorizationCheckerInterface $authorization)
    {
        $this->buildRepository = $em->getRepository(Build::class);
        $this->authorization = $authorization;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('BUILD_status_label', [$this, 'renderStatusLabel'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('BUILD_can_build', [$this, 'canBuild']),
            new \Twig_SimpleFunction('BUILD_can_edit', [$this, 'canEdit']),
            new \Twig_SimpleFunction('BUILD_can_delete', [$this, 'canDelete']),
        );
    }

    /**
     * @param Build $build
     * @return string
     */
    public function renderStatusLabel(Build $build)
    {
        $types = [
            Type::CODE_BUILD_STATUS_BUILT => 'info',
            Type::CODE_BUILD_STATUS_BUILDING => 'warning',
            Type::CODE_BUILD_STATUS_READY => 'success',
        ];

        return sprintf(
            '<span class="label label-%s">%s</span>',
            $types[$build->getStatusType()->getCode()],
            $build->getStatusType()
        );
    }

    /**
     * @param Build $build
     * @return bool
     */
    public function canBuild(Build $build)
    {
        return $this->authorization->isGranted('ROLE_BUILD_BUILD') && $this->buildRepository->canBuild($build);
    }

    /**
     * @param Build $build
     * @return bool
     */
    public function canEdit(Build $build)
    {
        return $this->authorization->isGranted('ROLE_BUILD_EDIT') && $this->buildRepository->canEdit($build);
    }

    /**
     * @param Build $build
     * @return bool
     */
    public function canDelete(Build $build)
    {
        return $this->authorization->isGranted('ROLE_BUILD_DELETE') && $this->buildRepository->canDelete($build);
    }

    public function getName()
    {
        return 'ci_app_build_extension';
    }

}