<?php

namespace CI\AppBundle\Twig;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class MessageExtension extends \Twig_Extension
{
    const SUCCESS = 'success';
    const INFO = 'info';
    const ERROR = 'danger';

    /**
     * @var SessionInterface
     */
    protected $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('MESSAGE_render', [$this, 'renderMessage'], ['needs_environment' => true, 'is_safe' => ['html']]),
            new \Twig_SimpleFunction('MESSAGE_single', [$this, 'renderSingle'], ['needs_environment' => true, 'is_safe' => ['html']])
        );
    }

    /**
     * @param \Twig_Environment $twig
     * @return string
     */
    public function renderMessage(\Twig_Environment $twig)
    {
        $messages = [];

        foreach ($this->session->getFlashBag()->all() as $type => $texts) {
            foreach ($texts as $text) {
                $messages[] = ['type' => $type, 'text' => $text];
            }
        }

        return $twig->render('CIAppBundle::message.html.twig', [
            'messages' => $messages
        ]);
    }

    /**
     * @param \Twig_Environment $twig
     * @param $type
     * @param $message
     * @return string
     */
    public function renderSingle(\Twig_Environment $twig, $type, $message)
    {
        return $twig->render('CIAppBundle::message.html.twig', ['messages' => [['type' => $type, 'text' => $message]]]);
    }

    public function getName()
    {
        return 'ci_app_message_extension';
    }

}