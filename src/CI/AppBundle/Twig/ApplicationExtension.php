<?php

namespace CI\AppBundle\Twig;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\Type;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ApplicationExtension extends \Twig_Extension
{
    /**
     * @var \CI\AppBundle\Repository\ApplicationRepository
     */
    protected $applicationRepository;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorization;

    public function __construct(EntityManagerInterface $em, AuthorizationCheckerInterface $authorization)
    {
        $this->applicationRepository = $em->getRepository(Application::class);
        $this->authorization = $authorization;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('APPLICATION_status_label', [$this, 'renderStatusLabel'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('APPLICATION_can_deploy', [$this, 'canDeploy']),
            new \Twig_SimpleFunction('APPLICATION_can_edit', [$this, 'canEdit'])
        );
    }

    /**
     * @param Application $application
     * @return string
     */
    public function renderStatusLabel(Application $application)
    {
        $types = [
            Type::CODE_APPLICATION_STATUS_AWAITING_PROCESSING => 'info',
            Type::CODE_APPLICATION_STATUS_READY => 'success',
        ];

        return sprintf(
            '<span class="label label-%s">%s</span>',
            $types[$application->getStatusType()->getCode()],
            $application->getStatusType()
        );
    }

    /**
     * @param Application $application
     * @return bool
     */
    public function canDeploy(Application $application)
    {
        return $this->authorization->isGranted('ROLE_APPLICATION_DEPLOY') && $this->applicationRepository->canDeploy($application);
    }

    /**
     * @param Application $application
     * @return bool
     */
    public function canEdit(Application $application)
    {
        return $this->authorization->isGranted('ROLE_APPLICATION_EDIT') && $this->applicationRepository->canEdit($application);
    }

    public function getName()
    {
        return 'ci_app_application_extension';
    }

}