<?php

namespace CI\AppBundle\Exception;

use Symfony\Component\Translation\TranslatorInterface;

class TransException extends \Exception
{
    /**
     * @var array
     */
    protected $parameters = [];
    
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
        
        return $this;
    }
    
    public function trans(TranslatorInterface $trans)
    {
        return $trans->trans($this->getMessage(), $this->parameters);
    }
}
