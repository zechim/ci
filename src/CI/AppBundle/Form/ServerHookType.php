<?php

namespace CI\AppBundle\Form;

use CI\AppBundle\Entity\Hook;
use CI\AppBundle\Entity\ServerHook;
use CI\AppBundle\Entity\Type;
use CI\AppBundle\Repository\TypeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class ServerHookType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('hook', EntityType::class, [
            'label' => 'trans.entity.hook',
            'constraints' => new NotNull(),
            'class' => Hook::class,
        ]);

        $builder->add('hookPositionType', EntityType::class, [
            'label' => 'trans.entity.hookPositionType',
            'class' => Type::class,
            'constraints' => new NotNull(),
            'query_builder' => function (TypeRepository $repo) {
                return $repo->getHookPositionTypeAsQueryBuilder();
            },
        ]);

        $builder->add('hookType', EntityType::class, [
            'label' => 'trans.entity.hookType',
            'class' => Type::class,
            'constraints' => new NotNull(),
            'query_builder' => function (TypeRepository $repo) {
                return $repo->getHookTypeAsQueryBuilder();
            },
        ]);

        $builder->add('position', TextType::class, [
            'label' => 'trans.entity.position'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ServerHook::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ci_appbundle_server_hook';
    }


}
