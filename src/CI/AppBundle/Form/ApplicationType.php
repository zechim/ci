<?php

namespace CI\AppBundle\Form;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Repository\ApplicationConfigRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\NotNull;

class ApplicationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, [
            'label' => 'trans.entity.name',
            'required' => true,
            'constraints' => new NotNull(),
        ]);

        $builder->add('applicationConfig', CollectionType::class, [
            'label' => 'trans.entity.config',
            'entry_type' => ApplicationConfigType::class
        ]);

        $builder->add('applicationUpload', CollectionType::class, [
            'label' => 'trans.entity.upload',
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false,
            'entry_type' => ApplicationUploadType::class
        ]);

        $builder->add('applicationHook', CollectionType::class, [
            'label' => 'trans.entity.hook',
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false,
            'entry_type' => ApplicationHookType::class
        ]);

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) use ($options) {
            foreach ($event->getForm()->get('applicationConfig') as $form) {
                if (true === $options['applicationConfigRepo']->isApplicationConfigValid($form->getData())) {
                    continue;
                }

                $form->get('value')->addError(new FormError($options['translator']->trans('trans.message.can.not.be.empty')));
            }
        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Application::class,
            'constraints' => [new UniqueEntity(['fields' => 'name'])]
        ));

        $resolver->setRequired(['applicationConfigRepo', 'translator']);
        $resolver->setAllowedTypes('applicationConfigRepo', [ApplicationConfigRepository::class]);
        $resolver->setAllowedTypes('translator', [TranslatorInterface::class]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ci_appbundle_application';
    }


}
