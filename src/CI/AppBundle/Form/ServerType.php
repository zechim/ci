<?php

namespace CI\AppBundle\Form;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Repository\ServerConfigRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\NotNull;

class ServerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, [
            'label' => 'trans.entity.name',
            'required' => true,
            'constraints' => new NotNull(),
        ]);

        $builder->add('application', EntityType::class, [
            'class' => Application::class,
            'label' => 'trans.entity.application',
            'constraints' => new NotNull(),
        ]);

        $builder->add('serverConfig', CollectionType::class, [
            'label' => 'trans.entity.config',
            'entry_type' => ServerConfigType::class
        ]);

        $builder->add('serverUpload', CollectionType::class, [
            'label' => 'trans.entity.upload',
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false,
            'entry_type' => ServerUploadType::class
        ]);

        $builder->add('serverHook', CollectionType::class, [
            'label' => 'trans.entity.hookList',
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false,
            'entry_type' => ServerHookType::class
        ]);

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) use ($options) {
            foreach ($event->getForm()->get('serverConfig') as $form) {
                if (true === $options['serverConfigRepo']->isServerConfigValid($form->getData())) {
                    continue;
                }

                $form->get('value')->addError(new FormError($options['translator']->trans('trans.message.can.not.be.empty')));
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Server::class,
            'constraints' => [new UniqueEntity(['fields' => 'name'])]
        ));

        $resolver->setRequired(['serverConfigRepo', 'translator']);
        $resolver->setAllowedTypes('serverConfigRepo', [ServerConfigRepository::class]);
        $resolver->setAllowedTypes('translator', [TranslatorInterface::class]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ci_appbundle_server';
    }


}
