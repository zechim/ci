<?php

namespace CI\AppBundle\Form;

use CI\AppBundle\Entity\ServerConfig;
use CI\AppBundle\Entity\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServerConfigType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('configType', EntityType::class, [
            'class' => Type::class,
            'label' => 'trans.entity.parameter',
            'disabled' => true,
        ]);

        $builder->add('value', TextareaType::class, [
            'label' => 'trans.entity.value'
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ServerConfig::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ci_appbundle_server_config';
    }


}
