<?php

namespace CI\AppBundle\Form;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\Build;
use CI\AppBundle\Repository\ApplicationRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class BuildType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('application', EntityType::class, [
            'class' => Application::class,
            'label' => 'trans.entity.application',
            'placeholder' => 'trans.message.choose_an_option',
            'query_builder' => function (ApplicationRepository $repo) {
                return $repo->getAvailableAsQueryBuilder();
            }
        ]);

        $builder->add('branch', TextType::class, [
            'label' => 'trans.entity.branch',
            'constraints' => new NotNull(),
        ]);

        $builder->add('repo', TextType::class, [
            'label' => 'trans.entity.repo',
            'constraints' => new NotNull(),
        ]);

        $builder->add('script', TextareaType::class, [
            'label' => 'trans.entity.script',
        ]);

        $builder->add('autoDeploy', ChoiceType::class, [
            'label' => 'trans.entity.autoDeploy',
            'choices' => [
                'trans.message.no' => 0,
                'trans.message.yes' => 1,
            ]
        ]);

        $builder->add('buildUpload', CollectionType::class, [
            'label' => 'trans.entity.upload',
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'by_reference' => false,
            'entry_type' => BuildUploadType::class
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Build::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ci_appbundle_build';
    }


}
