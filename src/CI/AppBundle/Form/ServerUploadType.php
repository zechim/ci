<?php

namespace CI\AppBundle\Form;

use CI\AppBundle\Entity\ServerUpload;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class ServerUploadType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('targetPath', TextType::class, [
            'label' => 'trans.entity.targetPath',
            'constraints' => new NotNull(),
        ]);

        $builder->add('value', TextareaType::class, [
            'label' => 'trans.entity.value',
            'constraints' => new NotNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ServerUpload::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ci_appbundle_server_upload';
    }


}
