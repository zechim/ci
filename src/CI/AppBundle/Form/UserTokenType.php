<?php

namespace CI\AppBundle\Form;

use CI\AppBundle\Entity\UserToken;
use CI\AppBundle\Form\Transform\DateTimeTransform;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserTokenType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('isActive', ChoiceType::class, [
            'label' => 'trans.entity.active',
            'expanded' => true,
            'choices' => [
                'trans.message.yes' => 1,
                'trans.message.no' => 0
            ],
            'attr' => [
                'class' => 'form-control',
            ],
            'constraints' => [new NotBlank()]
        ]);

        $builder->add('token', null, [
            'label' => 'trans.entity.token',
            'attr' => [
                'class' => 'form-control',
            ],
            'constraints' => [new NotBlank()]
        ]);

        $builder->add('expiresAt', DateTimeType::class, [
            'label' => 'trans.entity.expiresAt',
            'widget' => 'single_text',
            'attr' => [
                'class' => 'form-control',
            ],
        ]);

        $builder->add(
            $builder->create('expiresAt', null, [
                'label' => 'trans.entity.expiresAt',
                'attr' => ['class' => 'form-control date'],
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy'
            ])->addModelTransformer(new DateTimeTransform())
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => UserToken::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ci_appbundle_user_permission';
    }
}
