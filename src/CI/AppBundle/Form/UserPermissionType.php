<?php

namespace CI\AppBundle\Form;

use CI\AppBundle\Entity\UserAction;
use CI\AppBundle\Entity\UserPermission;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserPermissionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, [
            'label' => 'trans.entity.name',
            'attr' => ['class' => 'form-control'],
        ]);

        $builder->add('userAction', EntityType::class, [
            'label' => 'trans.entity.user_action',
            'expanded' => false,
            'multiple' => true,
            'empty_data' => null,
            'class' => UserAction::class,
            'attr' => ['class' => 'form-control']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => UserPermission::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ci_appbundle_user_permission';
    }
}
