<?php

namespace CI\AppBundle\Form\Transform;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class DateTimeTransform implements DataTransformerInterface
{
    protected $format = 'd/m/Y';

    public function __construct($format = 'd/m/Y')
    {
        $this->format = $format;
    }

    /**
     * @param mixed $value
     * @return mixed|null
     */
    public function transform($value)
    {
        if (true === empty($value)) {
            return null;
        }

        return $value;
    }

    /**
     * @param mixed $value
     * @return array|\DateTime|mixed
     */
    public function reverseTransform($value)
    {
        if (true === empty($value)) {
            return null;
        }

        if ($value instanceof \DateTime) {
            return $value;
        }

        return \DateTime::createFromFormat($this->format, $value, date_default_timezone_get());
    }
}
 