<?php

namespace CI\AppBundle\Entity;

/**
 * Queue
 */
class Queue
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var array
     */
    private $request;

    /**
     * @var string
     */
    private $service;

    /**
     * @var array
     */
    private $response;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $executedAt;

    /**
     * @var \DateTime
     */
    private $scheduledAt;

    /**
     * @var History
     */
    private $history;

    /**
     * @var Type
     */
    private $statusType;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set request
     *
     * @param array $request
     *
     * @return Queue
     */
    public function setRequest(array $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return array
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set service
     *
     * @param string $service
     *
     * @return Queue
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set response
     *
     * @param array $response
     *
     * @return Queue
     */
    public function setResponse(array $response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get response
     *
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Queue
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set executedAt
     *
     * @param \DateTime $executedAt
     *
     * @return Queue
     */
    public function setExecutedAt($executedAt)
    {
        $this->executedAt = $executedAt;

        return $this;
    }

    /**
     * Get executedAt
     *
     * @return \DateTime
     */
    public function getExecutedAt()
    {
        return $this->executedAt;
    }

    /**
     * Set scheduledAt
     *
     * @param \DateTime $scheduledAt
     *
     * @return Queue
     */
    public function setScheduledAt($scheduledAt)
    {
        $this->scheduledAt = $scheduledAt;

        return $this;
    }

    /**
     * Get scheduledAt
     *
     * @return \DateTime
     */
    public function getScheduledAt()
    {
        return $this->scheduledAt;
    }

    /**
     * Set history
     *
     * @param History $history
     *
     * @return Queue
     */
    public function setHistory(History $history = null)
    {
        $this->history = $history;

        return $this;
    }

    /**
     * Get history
     *
     * @return History
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * Set statusType
     *
     * @param Type $statusType
     *
     * @return Queue
     */
    public function setStatusType(Type $statusType = null)
    {
        $this->statusType = $statusType;

        return $this;
    }

    /**
     * Get statusType
     *
     * @return Type
     */
    public function getStatusType()
    {
        return $this->statusType;
    }
}
