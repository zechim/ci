<?php

namespace CI\AppBundle\Entity;

/**
 * ServerHook
 */
class ServerHook
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var integer
     */
    private $position;

    /**
     * @var \CI\AppBundle\Entity\Server
     */
    private $server;

    /**
     * @var Hook
     */
    private $hook;

    /**
     * @var \CI\AppBundle\Entity\Type
     */
    private $hookPositionType;

    /**
     * @var \CI\AppBundle\Entity\Type
     */
    private $hookType;

    /**
     * @var \CI\AppBundle\Entity\User
     */
    private $createdBy;

    /**
     * @var \CI\AppBundle\Entity\User
     */
    private $updatedBy;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hook
     *
     * @param Hook $hook
     *
     * @return ServerHook
     */
    public function setHook(Hook $hook)
    {
        $this->hook = $hook;

        return $this;
    }

    /**
     * Get hook
     *
     * @return Hook
     */
    public function getHook()
    {
        return $this->hook;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ServerHook
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ServerHook
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set server
     *
     * @param \CI\AppBundle\Entity\Server $server
     *
     * @return ServerHook
     */
    public function setServer(\CI\AppBundle\Entity\Server $server = null)
    {
        $this->server = $server;

        return $this;
    }

    /**
     * Get server
     *
     * @return \CI\AppBundle\Entity\Server
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * Set hookPositionType
     *
     * @param \CI\AppBundle\Entity\Type $hookPositionType
     *
     * @return ServerHook
     */
    public function setHookPositionType(\CI\AppBundle\Entity\Type $hookPositionType = null)
    {
        $this->hookPositionType = $hookPositionType;

        return $this;
    }

    /**
     * Get hookPositionType
     *
     * @return \CI\AppBundle\Entity\Type
     */
    public function getHookPositionType()
    {
        return $this->hookPositionType;
    }

    /**
     * Set hookType
     *
     * @param \CI\AppBundle\Entity\Type $hookType
     *
     * @return ServerHook
     */
    public function setHookType(\CI\AppBundle\Entity\Type $hookType = null)
    {
        $this->hookType = $hookType;

        return $this;
    }

    /**
     * Get hookType
     *
     * @return \CI\AppBundle\Entity\Type
     */
    public function getHookType()
    {
        return $this->hookType;
    }

    /**
     * Set createdBy
     *
     * @param \CI\AppBundle\Entity\User $createdBy
     *
     * @return ServerHook
     */
    public function setCreatedBy(\CI\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \CI\AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \CI\AppBundle\Entity\User $updatedBy
     *
     * @return ServerHook
     */
    public function setUpdatedBy(\CI\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \CI\AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return ServerHook
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s - %s - %s', $this->hook, $this->hookType, $this->hookPositionType);
    }
}
