<?php

namespace CI\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Server
 */
class Server
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $serverConfig;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $history;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $serverUpload;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $serverHook;

    /**
     * @var Application
     */
    private $application;

    /**
     * @var Type
     */
    private $statusType;

    /**
     * @var User
     */
    private $createdBy;

    /**
     * @var User
     */
    private $updatedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->serverConfig = new ArrayCollection();
        $this->history = new ArrayCollection();
        $this->serverUpload = new ArrayCollection();
        $this->serverHook = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Server
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Server
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Server
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add serverConfig
     *
     * @param ServerConfig $serverConfig
     *
     * @return Server
     */
    public function addServerConfig(ServerConfig $serverConfig)
    {
        if (false === $this->serverConfig->contains($serverConfig)) {
            $serverConfig->setServer($this);
            $this->serverConfig[] = $serverConfig;
        }

        return $this;
    }

    /**
     * Remove serverConfig
     *
     * @param ServerConfig $serverConfig
     */
    public function removeServerConfig(ServerConfig $serverConfig)
    {
        $this->serverConfig->removeElement($serverConfig);
    }

    /**
     * @param null $code
     * @return string|null|\Doctrine\Common\Collections\Collection
     */
    public function getServerConfig($code = null)
    {
        if (null !== $code) {
            /** @var ApplicationConfig $applicationConfig */
            $config = $this->serverConfig->filter(function (ServerConfig $config) use ($code) {
                return $config->getConfigType()->getCode() === $code;
            })->first();

            if (false === $config instanceof ServerConfig || trim($config->getValue()) === '') {
                return $this->application->getApplicationConfig($code);
            }

            return $config->getValue();
        }

        return $this->serverConfig;
    }

    /**
     * Add serverUpload
     *
     * @param ServerUpload $serverUpload
     *
     * @return Server
     */
    public function addServerUpload(ServerUpload $serverUpload)
    {
        if (false === $this->serverUpload->contains($serverUpload)) {
            $serverUpload->setServer($this);
            $this->serverUpload[] = $serverUpload;
        }

        return $this;
    }

    /**
     * Remove serverUpload
     *
     * @param ServerUpload $serverUpload
     */
    public function removeServerUpload(ServerUpload $serverUpload)
    {
        $this->serverUpload->removeElement($serverUpload);
    }

    /**
     * @param bool $full
     * @return ArrayCollection|\Doctrine\Common\Collections\Collection
     */
    public function getServerUpload($full = false)
    {
        if (true === $full) {
            $uploads = new ArrayCollection();

            foreach ($this->getApplication()->getApplicationUpload() as $upload) {
                $uploads->set($upload->getTargetPath(), $upload);
            }

            foreach ($this->serverUpload as $upload) {
                $uploads->set($upload->getTargetPath(), $upload);
            }

            return $uploads;
        }

        return $this->serverUpload;
    }

    /**
     * Add history
     *
     * @param History $history
     *
     * @return Server
     */
    public function addHistory(History $history)
    {
        if (false === $this->history->contains($history)) {
            $history->setApplication($this);
            $this->history[] = $history;
        }

        return $this;
    }

    /**
     * Remove history
     *
     * @param History $history
     */
    public function removeHistory(History $history)
    {
        $this->history->removeElement($history);
    }

    /**
     * Get history
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistory()
    {
        return $this->history;
    }


    /**
     * Add serverHook
     *
     * @param ServerHook $serverHook
     *
     * @return Server
     */
    public function addServerHook(ServerHook $serverHook)
    {
        if (false === $this->serverHook->contains($serverHook)) {
            $serverHook->setServer($this);
            $this->serverHook[] = $serverHook;
        }

        return $this;
    }

    /**
     * Remove serverHook
     *
     * @param ServerHook $serverHook
     */
    public function removeServerHook(ServerHook $serverHook)
    {
        $this->serverHook->removeElement($serverHook);
    }

    /**
     * @param bool $full
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServerHook($full = false)
    {
        if (true === $full) {
            $hooks = new ArrayCollection();

            $getHash = function ($hook) {
                return sprintf(
                    '%s-%s-%s',
                    $hook->getHook()->getId(),
                    $hook->getHookType()->getId(),
                    $hook->getHookPositionType()->getId()
                );
            };

            /* @var ApplicationHook $hook */
            foreach ($this->getApplication()->getApplicationHook() as $hook) {
                $hooks->set($getHash($hook), $hook);
            }

            /* @var ServerHook $hook */
            foreach ($this->serverHook as $hook) {
                $hooks->set($getHash($hook), $hook);
            }

            return $hooks;
        }

        return $this->serverHook;
    }

    /**
     * Set application
     *
     * @param Application $application
     *
     * @return Server
     */
    public function setApplication(Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set statusType
     *
     * @param Type $statusType
     *
     * @return Server
     */
    public function setStatusType(Type $statusType = null)
    {
        $this->statusType = $statusType;

        return $this;
    }

    /**
     * Get statusType
     *
     * @return Type
     */
    public function getStatusType()
    {
        return $this->statusType;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return Server
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param User $updatedBy
     *
     * @return Server
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s (%s)', $this->name, $this->getServerConfig(Type::CODE_CONFIG_IP));
    }
}
