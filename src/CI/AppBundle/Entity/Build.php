<?php

namespace CI\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * Build
 */
class Build
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var array
     */
    private $response;

    /**
     * @var string
     */
    private $script;

    /**
     * @var string
     */
    private $branch;

    /**
     * @var string
     */
    private $repo;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var Type
     */
    private $statusType;

    /**
     * @var User
     */
    private $createdBy;

    /**
     * @var User
     */
    private $updatedBy;

    /**
     * @var ArrayCollection
     */
    private $buildUpload;

    /**
     * @var ArrayCollection
     */
    private $tag;

    /**
     * @var boolean
     */
    private $autoDeploy;

    /**
     * @var Application
     */
    private $application;
    
    public function __construct()
    {
        $this->createdAt = new \DateTime;
        $this->updatedAt = new \DateTime;
        $this->buildUpload = new ArrayCollection;
        $this->tag = new ArrayCollection;
        $this->autoDeploy = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set response
     *
     * @param array $response
     *
     * @return Build
     */
    public function setResponse(array $response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get response
     *
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set script
     *
     * @param string $script
     *
     * @return Build
     */
    public function setScript($script)
    {
        $this->script = $script;

        return $this;
    }

    /**
     * Get script
     *
     * @return string
     */
    public function getScript()
    {
        return $this->script;
    }

    /**
     * Set branch
     *
     * @param string $branch
     *
     * @return Build
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;

        return $this;
    }

    /**
     * Get branch
     *
     * @return string
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * Set repo
     *
     * @param string $repo
     *
     * @return Build
     */
    public function setRepo($repo)
    {
        $this->repo = $repo;

        return $this;
    }

    /**
     * Get repo
     *
     * @return string
     */
    public function getRepo()
    {
        return $this->repo;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Build
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Build
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set statusType
     *
     * @param Type $statusType
     *
     * @return Build
     */
    public function setStatusType(Type $statusType = null)
    {
        $this->statusType = $statusType;

        return $this;
    }

    /**
     * Get statusType
     *
     * @return Type
     */
    public function getStatusType()
    {
        return $this->statusType;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return Build
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param User $updatedBy
     *
     * @return Build
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Add buildUpload
     *
     * @param BuildUpload $buildUpload
     *
     * @return Build
     */
    public function addBuildUpload(BuildUpload $buildUpload)
    {
        if (false === $this->buildUpload->contains($buildUpload)) {
            $buildUpload->setBuild($this);
            $this->buildUpload[] = $buildUpload;
        }

        return $this;
    }

    /**
     * @return BuildUpload|ArrayCollection
     */
    public function getBuildUpload()
    {
        return $this->buildUpload;
    }

    /**
     * Remove buildUpload
     *
     * @param BuildUpload $buildUpload
     */
    public function removeBuildUpload(BuildUpload $buildUpload)
    {
        $this->buildUpload->removeElement($buildUpload);
    }

    /**
     * Add tag
     *
     * @param Tag $tag
     *
     * @return Build
     */
    public function addTag(Tag $tag)
    {
        if (false === $this->tag->contains($tag)) {
            $tag->setBuild($this);
            $this->tag->add($tag);
        }

        return $this;
    }

    /**
     * @return Tag|ArrayCollection
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Remove tag
     *
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tag->removeElement($tag);
    }

    /**
     * Set autoDeploy
     *
     * @param boolean $autoDeploy
     *
     * @return Build
     */
    public function setAutoDeploy($autoDeploy)
    {
        $this->autoDeploy = $autoDeploy;

        return $this;
    }

    /**
     * Get autoDeploy
     *
     * @return boolean
     */
    public function getAutoDeploy()
    {
        return $this->autoDeploy;
    }

    /**
     * Set application
     *
     * @param Application $application
     *
     * @return Build
     */
    public function setApplication(Application $application = null)
    {
        if (null !== $application) {
            $application->setBuild($this);
        }

        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @return null|Tag
     */
    public function getCurrentTag()
    {
        $tags = $this->tag->toArray();

        usort($tags, function(Tag $left, Tag $right) {
           return $left->getId() > $right->getId() ? -1 : 1;
        });

        return reset($tags);
    }

    /**
     * @return null|string
     */
    public function getCurrentTagName()
    {
        $tag = $this->getCurrentTag();

        if (false === $tag instanceof Tag) {
            return null;
        }

        return $tag->getName();
    }

    /**
     * @return null|string
     */
    public function getCurrentTagCommit()
    {
        $tag = $this->getCurrentTag();

        if (false === $tag instanceof Tag) {
            return null;
        }

        return $tag->getCommit();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('BUILD %s', $this->getId());
    }
}
