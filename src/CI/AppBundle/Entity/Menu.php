<?php

namespace CI\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Menu
 */
class Menu
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $pageName;

    /**
     * @var string
     */
    private $menuName;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var string
     */
    private $route;

    /**
     * @var boolean
     */
    private $position;

    /**
     * @var boolean
     */
    private $isVisible;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @var Menu
     */
    private $parent;

    /**
     * @var UserAction
     */
    private $userAction;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pageName
     *
     * @param string $pageName
     * @return Menu
     */
    public function setPageName($pageName)
    {
        $this->pageName = $pageName;

        return $this;
    }

    /**
     * Get pageName
     *
     * @return string 
     */
    public function getPageName()
    {
        return $this->pageName;
    }

    /**
     * Set menuName
     *
     * @param string $menuName
     * @return Menu
     */
    public function setMenuName($menuName)
    {
        $this->menuName = $menuName;

        return $this;
    }

    /**
     * Get menuName
     *
     * @return string 
     */
    public function getMenuName()
    {
        return $this->menuName;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Menu
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set route
     *
     * @param string $route
     * @return Menu
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route
     *
     * @return string 
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set boolean
     *
     * @param boolean $position
     * @return Menu
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return boolean
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set boolean
     *
     * @param boolean $isVisible
     * @return Menu
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * Get isVisible
     *
     * @return boolean
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }

    /**
     * Add children
     *
     * @param Menu $child
     * @return Menu
     */
    public function addChildren(Menu $child)
    {
        if (false === $this->children->contains($child)) {
            $child->setParent($this);
            $this->children[] = $child;
        }

        return $this;
    }

    /**
     * Remove children
     *
     * @param Menu $children
     */
    public function removeChildren(Menu $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set menu
     *
     * @param Menu $parent
     * @return Menu
     */
    public function setParent(Menu $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get menu
     *
     * @return Menu 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set userAction
     *
     * @param UserAction $userAction
     * @return Menu
     */
    public function setUserAction(UserAction $userAction = null)
    {
        $this->userAction = $userAction;

        return $this;
    }

    /**
     * Get userAction
     *
     * @return UserAction 
     */
    public function getUserAction()
    {
        return $this->userAction;
    }
}
