<?php

namespace CI\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * UserAction
 */
class UserAction
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $referenceClass;

    /**
     * @var string
     */
    private $referenceValue;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userPermission;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userPermission = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return UserAction
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return UserAction
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set referenceClass
     *
     * @param string $referenceClass
     * @return UserAction
     */
    public function setReferenceClass($referenceClass)
    {
        $this->referenceClass = $referenceClass;

        return $this;
    }

    /**
     * Get referenceClass
     *
     * @return string
     */
    public function getReferenceClass()
    {
        return $this->referenceClass;
    }

    /**
     * Set referenceValue
     *
     * @param string $referenceValue
     * @return UserAction
     */
    public function setReferenceValue($referenceValue)
    {
        $this->referenceValue = $referenceValue;

        return $this;
    }

    /**
     * Get referenceValue
     *
     * @return string
     */
    public function getReferenceValue()
    {
        return $this->referenceValue;
    }

    /**
     * Add userPermission
     *
     * @param UserPermission $userPermission
     * @return UserAction
     */
    public function addUserPermission(UserPermission $userPermission)
    {
        if (false === $this->userPermission->contains($userPermission)) {
            $this->userPermission[] = $userPermission;
        }

        return $this;
    }

    /**
     * Remove userPermission
     *
     * @param UserPermission $userPermission
     */
    public function removeUserPermission(UserPermission $userPermission)
    {
        $this->userPermission->removeElement($userPermission);
    }

    /**
     * Get userPermission
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserPermission()
    {
        return $this->userPermission;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }
}
