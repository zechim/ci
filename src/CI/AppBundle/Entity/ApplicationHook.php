<?php

namespace CI\AppBundle\Entity;

/**
 * ApplicationHook
 */
class ApplicationHook
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var integer
     */
    private $position;

    /**
     * @var \CI\AppBundle\Entity\Application
     */
    private $application;

    /**
     * @var Hook
     */
    private $hook;

    /**
     * @var \CI\AppBundle\Entity\Type
     */
    private $hookPositionType;

    /**
     * @var \CI\AppBundle\Entity\Type
     */
    private $hookType;

    /**
     * @var \CI\AppBundle\Entity\User
     */
    private $createdBy;

    /**
     * @var \CI\AppBundle\Entity\User
     */
    private $updatedBy;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hook
     *
     * @param Hook $hook
     *
     * @return ApplicationHook
     */
    public function setHook(Hook $hook)
    {
        $this->hook = $hook;

        return $this;
    }

    /**
     * Get hook
     *
     * @return Hook
     */
    public function getHook()
    {
        return $this->hook;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ApplicationHook
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ApplicationHook
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set application
     *
     * @param \CI\AppBundle\Entity\Application $application
     *
     * @return ApplicationHook
     */
    public function setApplication(\CI\AppBundle\Entity\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \CI\AppBundle\Entity\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set hookPositionType
     *
     * @param \CI\AppBundle\Entity\Type $hookPositionType
     *
     * @return ApplicationHook
     */
    public function setHookPositionType(\CI\AppBundle\Entity\Type $hookPositionType = null)
    {
        $this->hookPositionType = $hookPositionType;

        return $this;
    }

    /**
     * Get hookPositionType
     *
     * @return \CI\AppBundle\Entity\Type
     */
    public function getHookPositionType()
    {
        return $this->hookPositionType;
    }

    /**
     * Set hookType
     *
     * @param \CI\AppBundle\Entity\Type $hookType
     *
     * @return ApplicationHook
     */
    public function setHookType(\CI\AppBundle\Entity\Type $hookType = null)
    {
        $this->hookType = $hookType;

        return $this;
    }

    /**
     * Get hookType
     *
     * @return \CI\AppBundle\Entity\Type
     */
    public function getHookType()
    {
        return $this->hookType;
    }

    /**
     * Set createdBy
     *
     * @param \CI\AppBundle\Entity\User $createdBy
     *
     * @return ApplicationHook
     */
    public function setCreatedBy(\CI\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \CI\AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \CI\AppBundle\Entity\User $updatedBy
     *
     * @return ApplicationHook
     */
    public function setUpdatedBy(\CI\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \CI\AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return ApplicationHook
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s - %s - %s', $this->hook, $this->hookType, $this->hookPositionType);
    }
}
