<?php

namespace CI\AppBundle\Entity;

/**
 * Type
 */
class Type
{
    /* Server.configType or Application.configType */
    const CODE_CONFIG_BRANCH = 'CONFIG_BRANCH';
    const CODE_CONFIG_KEY = 'CONFIG_KEY';
    const CODE_CONFIG_REPO = 'CONFIG_REPO';
    const CODE_CONFIG_DEPLOY_TO = 'CONFIG_DEPLOY_TO';
    const CODE_CONFIG_IP = 'CONFIG_IP';
    const CODE_CONFIG_USER = 'CONFIG_USER';

    /* Server.statusType */
    const CODE_SERVER_STATUS_AWAITING_PROCESSING = 'SERVER_STATUS_AWAITING_PROCESSING';
    const CODE_SERVER_STATUS_DUMPING = 'SERVER_STATUS_DUMPING';
    const CODE_SERVER_STATUS_DUMPED = 'SERVER_STATUS_DUMPED';
    const CODE_SERVER_STATUS_DEPLOYING = 'SERVER_STATUS_DEPLOYING';
    const CODE_SERVER_STATUS_RUNNING_HOOK = 'SERVER_STATUS_RUNNING_HOOK';
    const CODE_SERVER_STATUS_DEPLOYED = 'SERVER_STATUS_DEPLOYED';
    const CODE_SERVER_STATUS_HOOK_RAN = 'SERVER_STATUS_HOOK_RAN';
    const CODE_SERVER_STATUS_READY = 'SERVER_STATUS_READY';

    /* Application.statusType */
    const CODE_APPLICATION_STATUS_AWAITING_PROCESSING = 'APPLICATION_STATUS_AWAITING_PROCESSING';
    const CODE_APPLICATION_STATUS_READY = 'APPLICATION_STATUS_READY';

    /* Queue.statusType */
    const CODE_QUEUE_STATUS_WAITING = 'QUEUE_STATUS_WAITING';
    const CODE_QUEUE_STATUS_RUNNING = 'QUEUE_STATUS_RUNNING';
    const CODE_QUEUE_STATUS_EXECUTED = 'QUEUE_STATUS_EXECUTED';
    const CODE_QUEUE_STATUS_ERROR = 'QUEUE_STATUS_ERROR';

    /* Hook.hookType */
    const CODE_HOOK_DEPLOY_STARTING = 'HOOK_DEPLOY_STARTING';
    const CODE_HOOK_DEPLOY_STARTED = 'HOOK_DEPLOY_STARTED';
    const CODE_HOOK_DEPLOY_UPDATING = 'HOOK_DEPLOY_UPDATING';
    const CODE_HOOK_DEPLOY_UPDATED = 'HOOK_DEPLOY_UPDATED';
    const CODE_HOOK_DEPLOY_PUBLISHING = 'HOOK_DEPLOY_PUBLISHING';
    const CODE_HOOK_DEPLOY_PUBLISHED = 'HOOK_DEPLOY_PUBLISHED';
    const CODE_HOOK_DEPLOY_REVERTING = 'HOOK_DEPLOY_REVERTING';
    const CODE_HOOK_DEPLOY_REVERTED = 'HOOK_DEPLOY_REVERTED';
    const CODE_HOOK_DEPLOY_FINISHING = 'HOOK_DEPLOY_FINISHING';
    const CODE_HOOK_DEPLOY_FINISHING_ROLLBACK = 'HOOK_DEPLOY_FINISHING_ROLLBACK';
    const CODE_HOOK_DEPLOY_FINISHED = 'HOOK_DEPLOY_FINISHED';

    /* Hook.hookPositionType */
    const CODE_HOOK_POSITION_AFTER = 'HOOK_POSITION_AFTER';
    const CODE_HOOK_POSITION_BEFORE = 'HOOK_POSITION_BEFORE';

    /* Build.statusType */
    const CODE_BUILD_STATUS_READY = 'BUILD_STATUS_READY';
    const CODE_BUILD_STATUS_BUILDING = 'BUILD_STATUS_BUILDING';
    const CODE_BUILD_STATUS_BUILT = 'BUILD_STATUS_BUILT';

    /* Tag.statusType */
    const CODE_TAG_STATUS_READY = 'TAG_STATUS_READY';
    const CODE_TAG_STATUS_DELETING = 'TAG_STATUS_DELETING';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $code;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var User
     */
    private $createdBy;

    /**
     * @var User
     */
    private $updatedBy;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Type
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Type
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Type
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Type
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return Type
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param User $updatedBy
     *
     * @return Type
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }
}
