<?php

namespace CI\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Application
 */
class Application
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var ArrayCollection
     */
    private $applicationConfig;

    /**
     * @var ArrayCollection
     */
    private $applicationUpload;

    /**
     * @var ArrayCollection
     */
    private $applicationHook;

    /**
     * @var ArrayCollection
     */
    private $server;

    /**
     * @var ArrayCollection
     */
    private $history;

    /**
     * @var Build
     */
    private $build;

    /**
     * @var Type
     */
    private $statusType;

    /**
     * @var User
     */
    private $createdBy;

    /**
     * @var User
     */
    private $updatedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->applicationConfig = new ArrayCollection();
        $this->applicationUpload = new ArrayCollection();
        $this->applicationHook = new ArrayCollection();
        $this->server = new ArrayCollection();
        $this->history = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Application
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Application
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Application
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add applicationConfig
     *
     * @param ApplicationConfig $applicationConfig
     *
     * @return Application
     */
    public function addApplicationConfig(ApplicationConfig $applicationConfig)
    {
        if (false === $this->applicationConfig->contains($applicationConfig)) {
            $applicationConfig->setApplication($this);

            $this->applicationConfig[] = $applicationConfig;
        }

        return $this;
    }

    /**
     * Remove applicationConfig
     *
     * @param ApplicationConfig $applicationConfig
     */
    public function removeApplicationConfig(ApplicationConfig $applicationConfig)
    {
        $this->applicationConfig->removeElement($applicationConfig);
    }

    /**
     * @param null $code
     * @param boolean $returnConfig
     * @return string|null|ArrayCollection|ApplicationConfig
     */
    public function getApplicationConfig($code = null, $returnConfig = false)
    {
        if (null !== $code) {
            /** @var ApplicationConfig $config */
            $config = $this->applicationConfig->filter(function (ApplicationConfig $config) use ($code) {
                return $config->getConfigType()->getCode() === $code;
            })->first();

            if (false === $config instanceof ApplicationConfig) {
                return null;
            }

            if (true === $returnConfig) {
                return $config;
            }

            return $config->getValue();
        }

        return $this->applicationConfig;
    }

    /**
     * Add applicationUpload
     *
     * @param ApplicationUpload $applicationUpload
     *
     * @return Application
     */
    public function addApplicationUpload(ApplicationUpload $applicationUpload)
    {
        if (false === $this->applicationUpload->contains($applicationUpload)) {
            $applicationUpload->setApplication($this);
            $this->applicationUpload[] = $applicationUpload;
        }

        return $this;
    }

    /**
     * Remove applicationUpload
     *
     * @param ApplicationUpload $applicationUpload
     */
    public function removeApplicationUpload(ApplicationUpload $applicationUpload)
    {
        $this->applicationUpload->removeElement($applicationUpload);
    }

    /**
     * @return ApplicationUpload|null|ArrayCollection
     */
    public function getApplicationUpload()
    {
        return $this->applicationUpload;
    }


    /**
     * Add applicationHook
     *
     * @param ApplicationHook $applicationHook
     *
     * @return Application
     */
    public function addApplicationHook(ApplicationHook $applicationHook)
    {
        if (false === $this->applicationHook->contains($applicationHook)) {
            $applicationHook->setApplication($this);
            $this->applicationHook[] = $applicationHook;
        }

        return $this;
    }

    /**
     * Remove applicationHook
     *
     * @param ApplicationHook $applicationHook
     */
    public function removeApplicationHook(ApplicationHook $applicationHook)
    {
        $this->applicationHook->removeElement($applicationHook);
    }

    /**
     * Get applicationHook
     *
     * @return ArrayCollection
     */
    public function getApplicationHook()
    {
        return $this->applicationHook;
    }

    /**
     * Add server
     *
     * @param Server $server
     *
     * @return Application
     */
    public function addServer(Server $server)
    {
        if (false === $this->server->contains($server)) {
            $server->setApplication($this);
            $this->server[] = $server;
        }

        return $this;
    }

    /**
     * Remove server
     *
     * @param Server $server
     */
    public function removeServer(Server $server)
    {
        $this->server->removeElement($server);
    }

    /**
     * Get server
     *
     * @return ArrayCollection
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * Add history
     *
     * @param History $history
     *
     * @return Application
     */
    public function addHistory(History $history)
    {
        if (false === $this->history->contains($history)) {
            $history->setApplication($this);
            $this->history[] = $history;
        }

        return $this;
    }

    /**
     * Remove history
     *
     * @param History $history
     */
    public function removeHistory(History $history)
    {
        $this->history->removeElement($history);
    }

    /**
     * Get history
     *
     * @return ArrayCollection
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * Set build
     *
     * @param Build $build
     *
     * @return Application
     */
    public function setBuild(Build $build = null)
    {
        $this->build = $build;

        return $this;
    }

    /**
     * Get build
     *
     * @return Build
     */
    public function getBuild()
    {
        return $this->build;
    }

    /**
     * Set statusType
     *
     * @param Type $statusType
     *
     * @return Application
     */
    public function setStatusType(Type $statusType = null)
    {
        $this->statusType = $statusType;

        return $this;
    }

    /**
     * Get statusType
     *
     * @return Type
     */
    public function getStatusType()
    {
        return $this->statusType;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return Application
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param User $updatedBy
     *
     * @return Application
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }
}
