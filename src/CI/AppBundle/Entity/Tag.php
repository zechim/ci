<?php

namespace CI\AppBundle\Entity;

/**
 * Tag
 */
class Tag
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $branch;

    /**
     * @var string
     */
    private $repo;

    /**
     * @var string
     */
    private $commit;

    /**
     * @var integer
     */
    private $buildQuantity;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $builtAt;

    /**
     * @var Type
     */
    private $statusType;

    /**
     * @var Build
     */
    private $build;

    /**
     * @var User
     */
    private $createdBy;

    public function __construct()
    {
        $this->createdAt = new \DateTime;
        $this->buildQuantity = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set branch
     *
     * @param string $branch
     *
     * @return Tag
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;

        return $this;
    }

    /**
     * Get branch
     *
     * @return string
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * Set repo
     *
     * @param string $repo
     *
     * @return Tag
     */
    public function setRepo($repo)
    {
        $this->repo = $repo;

        return $this;
    }

    /**
     * Get repo
     *
     * @return string
     */
    public function getRepo()
    {
        return $this->repo;
    }

    /**
     * Set commit
     *
     * @param string $commit
     *
     * @return Tag
     */
    public function setCommit($commit)
    {
        $this->commit = $commit;

        return $this;
    }

    /**
     * Get commit
     *
     * @return string
     */
    public function getCommit()
    {
        return $this->commit;
    }

    /**
     * Set buildQuantity
     *
     * @param integer $buildQuantity
     *
     * @return Tag
     */
    public function setBuildQuantity($buildQuantity)
    {
        $this->buildQuantity = $buildQuantity;

        return $this;
    }

    /**
     * Get buildQuantity
     *
     * @return integer
     */
    public function getBuildQuantity()
    {
        return $this->buildQuantity;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Tag
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set builtAt
     *
     * @param \DateTime $builtAt
     *
     * @return Tag
     */
    public function setBuiltAt($builtAt)
    {
        $this->builtAt = $builtAt;

        return $this;
    }

    /**
     * Get builtAt
     *
     * @return \DateTime
     */
    public function getBuiltAt()
    {
        return $this->builtAt;
    }

    /**
     * Set statusType
     *
     * @param Type $statusType
     *
     * @return Tag
     */
    public function setStatusType(Type $statusType = null)
    {
        $this->statusType = $statusType;

        return $this;
    }

    /**
     * Get statusType
     *
     * @return Type
     */
    public function getStatusType()
    {
        return $this->statusType;
    }

    /**
     * Set build
     *
     * @param Build $build
     *
     * @return Tag
     */
    public function setBuild(Build $build)
    {
        $this->build = $build;

        return $this;
    }

    /**
     * Get build
     *
     * @return Build
     */
    public function getBuild()
    {
        return $this->build;
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return Tag
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s (%s@%s)', $this->name, $this->branch, $this->commit);
    }
}
