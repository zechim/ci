<?php

namespace CI\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * UserPermission
 */
class UserPermission
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $editable;

    /**
     * @var boolean
     */
    private $admin;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userAction;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userAction = new ArrayCollection();
        $this->user = new ArrayCollection();
        $this->editable = true;
        $this->admin = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return UserPermission
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set editable
     *
     * @param boolean $editable
     * @return UserPermission
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;

        return $this;
    }

    /**
     * Get editable
     *
     * @return string
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * Set admin
     *
     * @param boolean $admin
     * @return UserPermission
     */
    public function setAdmin($admin)
    {
        $this->editable = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return string
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Add userAction
     *
     * @param UserAction $userAction
     * @return UserPermission
     */
    public function addUserAction(UserAction $userAction)
    {
        if (false === $this->userAction->contains($userAction)) {
            $this->userAction[] = $userAction;
        }

        return $this;
    }

    /**
     * Remove userAction
     *
     * @param UserAction $userAction
     */
    public function removeUserAction(UserAction $userAction)
    {
        $this->userAction->removeElement($userAction);
    }

    /**
     * Get userAction
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserAction()
    {
        return $this->userAction;
    }

    /**
     * Add user
     *
     * @param User $user
     * @return UserPermission
     */
    public function addUser(User $user)
    {
        if (false === $this->user->contains($user)) {
            $this->user[] = $user;
        }

        return $this;
    }

    /**
     * Remove user
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }
}
