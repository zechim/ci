<?php

namespace CI\AppBundle\DependencyInjection;

use CI\AppBundle\Service\Listener\Slack\Listener;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $root = $treeBuilder->root('ci_app');
        $root->children()
            ->scalarNode('base_path')
                ->cannotBeEmpty()
                ->defaultValue('%kernel.root_dir%/../var/ci/')
            ->end()
            ->scalarNode('base_url')
                ->cannotBeEmpty()
            ->end()
            ->arrayNode('template')
                ->addDefaultsIfNotSet()
                ->children()
                    ->scalarNode('capfile')->cannotBeEmpty()->defaultValue('CIAppBundle:Deploy:Capfile.empty.twig')->end()
                    ->scalarNode('deploy')->cannotBeEmpty()->defaultValue('CIAppBundle:Deploy:deploy.rb.twig')->end()
                    ->scalarNode('stage')->cannotBeEmpty()->defaultValue('CIAppBundle:Deploy:stage.rb.twig')->end()
                    ->scalarNode('rake')->cannotBeEmpty()->defaultValue('CIAppBundle:Deploy:rake/rake.rake.twig')->end()
                ->end()
            ->end()
            ->arrayNode('listener')
                ->addDefaultsIfNotSet()
                ->children()
                    ->arrayNode('slack')
                        ->canBeEnabled()
                        ->children()
                            ->scalarNode('class')->cannotBeEmpty()->defaultValue(Listener::class)->end()
                            ->scalarNode('event')->cannotBeEmpty()->defaultValue('ci.deploy.event')->end()
                            ->arrayNode('parameters')
                                ->children()
                                    ->scalarNode('endpoint')->cannotBeEmpty()->end()
                                    ->scalarNode('pretext')->cannotBeEmpty()->defaultValue('Automatic deploy using CI')->end()
                                    ->scalarNode('channel')->cannotBeEmpty()->defaultValue('ci')->end()
                                    ->scalarNode('username')->cannotBeEmpty()->defaultValue('CI')->end()
                                    ->arrayNode('message')
                                        ->addDefaultsIfNotSet()
                                        ->children()
                                            ->scalarNode('error')->cannotBeEmpty()->defaultValue('Project was not deployed')->end()
                                            ->scalarNode('success')->cannotBeEmpty()->defaultValue('Project deployed successfully')->end()
                                        ->end()
                                    ->end()
                                    ->arrayNode('color')
                                        ->addDefaultsIfNotSet()
                                        ->children()
                                            ->scalarNode('error')->cannotBeEmpty()->defaultValue('#f35a00')->end()
                                            ->scalarNode('success')->cannotBeEmpty()->defaultValue('#7cd197')->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
