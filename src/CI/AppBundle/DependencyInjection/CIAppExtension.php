<?php

namespace CI\AppBundle\DependencyInjection;

use CI\AppBundle\Service\Deploy\ConfigBag;
use CI\AppBundle\Service\Deploy\Dumper;
use CI\AppBundle\Service\Deploy\Queue\Event\DeployEvent;
use CI\AppBundle\Service\Deploy\Structure\Structure;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class CIAppExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $definition = new Definition(ConfigBag::class);
        $definition->addArgument(new Reference('templating'));
        $definition->addArgument([
            ConfigBag::BASE_PATH => $config['base_path'],
            ConfigBag::TEMPLATE_CAPFILE => $config['template']['capfile'],
            ConfigBag::TEMPLATE_DEPLOY => $config['template']['deploy'],
            ConfigBag::TEMPLATE_STAGE => $config['template']['stage'],
            ConfigBag::TEMPLATE_RAKE_FILE => $config['template']['rake']
        ]);
        $container->setDefinition('ci_app.config', $definition);

        $definition = new Definition(Structure::class);
        $definition->addArgument(new Reference('ci_app.config'));
        $container->setDefinition('ci_app.structure', $definition);

        $definition = new Definition(Dumper::class);
        $definition->addArgument(new Reference('ci_app.structure'));
        $container->setDefinition('ci_app.dumper', $definition);

        $this->addListener($config, $container);
    }

    protected function addListener(array $config, ContainerBuilder $container)
    {
        if (false === is_array($config)) {
            return;
        }

        $availableEvents = [DeployEvent::NAME => 'deploy'];

        foreach ($config['listener'] as $name => $options) {
            if (false === $options['enabled']) {
                continue;
            }

            if (false === array_key_exists($options['event'], $availableEvents)) {
                continue;
            }

            $eventType = $availableEvents[$options['event']];
            $eventName = $options['event'];

            $definition = new Definition($options['class']);
            $definition->addMethodCall('setContainer', [new Reference('service_container')]);
            $definition->addMethodCall('setParameters', [$options['parameters'] + ['base_url' => $config['base_url']]]);
            $definition->addTag('kernel.event_listener', ['event' => $eventName, 'method' => sprintf('on%sEvent', ucfirst($eventType))]);

            $container->setDefinition(sprintf('ci_%s.%s.%s.listener', $eventName, $name, $eventType), $definition);
        }
    }
}
