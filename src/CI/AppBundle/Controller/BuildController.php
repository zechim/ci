<?php

namespace CI\AppBundle\Controller;

use CI\AppBundle\Entity\Build;
use CI\AppBundle\Entity\BuildUpload;
use CI\AppBundle\Form\BuildType;
use CI\AppBundle\Repository\BuildRepository;
use CI\AppBundle\Service\Pagination\Pagination;
use CI\AppBundle\Twig\MessageExtension;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BuildController extends Controller
{
    public function indexAction($page)
    {
        $pagination = $this->getDoctrine()
            ->getManager()
            ->getRepository('CIAppBundle:Build')
            ->getPagination($this->getUser(), $page);

        $pagination->set(Pagination::ROUTE, 'ci_app_build_index');

        return $this->render('CIAppBundle:Build:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    public function showAction(Build $build)
    {
        return $this->render('CIAppBundle:Build:show.html.twig', [
            'build' => $build,
            'response' => var_export($build->getResponse(), true)
        ]);
    }

    public function createAction(Request $request)
    {
        /** @var BuildRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Build::class);

        $form = $this->createForm(BuildType::class);

        $form->handleRequest($request);

        if ('POST' === $request->getMethod() && true === $form->isValid()) {
            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.created');
            $build = $repo->create($form->getData(), $this->getUser());

            return $this->redirectToRoute('ci_app_build_show', ['build' => $build->getId()]);
        }

        return $this->render('CIAppBundle:Build:create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request, Build $build)
    {
        $form = $this->createForm(BuildType::class, $build);

        $form->handleRequest($request);

        if ('POST' === $request->getMethod() && true === $form->isValid()) {
            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.edited');
            $this->getDoctrine()->getRepository(Build::class)->edit($form->getData(), $this->getUser());

            return $this->redirectToRoute('ci_app_build_show', ['build' => $build->getId()]);
        }

        return $this->render('CIAppBundle:Build:edit.html.twig', [
            'form' => $form->createView(),
            'build' => $build,
        ]);
    }

    public function deleteAction(Build $build)
    {
        try {
            $this->getDoctrine()->getRepository(Build::class)->delete($build, $this->getUser());

            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.deleted');

        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $this->get('translator')->trans($e->getMessage()));
        }

        return $this->redirectToRoute('ci_app_build_index');
    }

    public function buildAction(Build $build)
    {
        try {
            $this->getDoctrine()->getRepository(Build::class)->build($build, $this->getUser());

        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $e->getMessage());
        }

        return $this->redirectToRoute('ci_app_build_show', ['build' => $build->getId()]);
    }

    public function showBuildUploadAction(BuildUpload $buildUpload)
    {
        return $this->render('CIAppBundle:Build:show.build.upload.html.twig', [
            'buildUpload' => $buildUpload,
        ]);
    }
}
