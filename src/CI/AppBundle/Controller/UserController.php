<?php

namespace CI\AppBundle\Controller;

use CI\AppBundle\Entity\User;
use CI\AppBundle\Form\UserType;
use CI\AppBundle\Repository\UserRepository;
use CI\AppBundle\Service\Pagination\Pagination;
use CI\AppBundle\Twig\MessageExtension;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    public function indexAction($page)
    {
        $pagination = $this->getDoctrine()
            ->getManager()
            ->getRepository('CIAppBundle:User')
            ->getPagination($page);

        $pagination->set(Pagination::ROUTE, 'ci_app_user_index');

        return $this->render('CIAppBundle:User:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    public function showAction(User $user)
    {
        return $this->render('CIAppBundle:User:show.html.twig', [
            'user' => $user
        ]);
    }

    public function createAction(Request $request)
    {
        /** @var UserRepository $repo */
        $repo = $this->getDoctrine()->getRepository(User::class);

        $form = $this->createForm(
            UserType::class,
            null,
            [
                'encoder' => $this->get('security.encoder_factory'),
                'translator' => $this->get('translator')
            ]
        );

        $form->handleRequest($request);

        if ('POST' === $request->getMethod() && true === $form->isValid()) {
            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.created');
            $user = $repo->create($form->getData(), $this->getUser());

            return $this->redirectToRoute('ci_app_user_show', ['user' => $user->getId()]);
        }

        return $this->render('CIAppBundle:User:create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request, User $user)
    {
        $form = $this->createForm(
            UserType::class,
            $user,
            [
                'encoder' => $this->get('security.encoder_factory'),
                'translator' => $this->get('translator')
            ]
        );

        $form->handleRequest($request);

        if ('POST' === $request->getMethod() && true === $form->isValid()) {
            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.edited');
            $this->getDoctrine()->getRepository(User::class)->edit($form->getData(), $this->getUser());

            return $this->redirectToRoute('ci_app_user_show', ['user' => $user->getId()]);
        }

        return $this->render('CIAppBundle:User:edit.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    public function deleteAction(User $user)
    {
        try {
            $this->getDoctrine()->getRepository(User::class)->delete($user, $this->getUser());

            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.delete');

        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $this->get('translator')->trans($e->getMessage()));
        }

        return $this->redirectToRoute('ci_app_user_index');
    }
}
