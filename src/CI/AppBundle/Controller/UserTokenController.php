<?php

namespace CI\AppBundle\Controller;

use CI\AppBundle\Entity\User;
use CI\AppBundle\Entity\UserToken;
use CI\AppBundle\Form\UserTokenType;
use CI\AppBundle\Repository\UserRepository;
use CI\AppBundle\Repository\UserTokenRepository;
use CI\AppBundle\Service\Pagination\Pagination;
use CI\AppBundle\Twig\MessageExtension;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserTokenController extends Controller
{
    public function indexAction($page)
    {
        $pagination = $this->getDoctrine()
            ->getManager()
            ->getRepository('CIAppBundle:UserToken')
            ->getPagination($page);

        $pagination->set(Pagination::ROUTE, 'ci_app_user_token_index');

        return $this->render('CIAppBundle:UserToken:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    public function showAction(UserToken $userToken)
    {
        return $this->render('CIAppBundle:UserToken:show.html.twig', [
            'userToken' => $userToken
        ]);
    }

    public function createAction(Request $request, User $user)
    {
        if (null !== $user->getUserToken()) {
            return $this->redirectToRoute('ci_app_user_token_show', ['userToken' => $user->getUserToken()->getId()]);
        }

        /** @var UserTokenRepository $repo */
        $repo = $this->getDoctrine()->getRepository(UserToken::class);

        $form = $this->createForm(UserTokenType::class, $repo->getDefault($user));
        $form->handleRequest($request);

        if ('POST' === $request->getMethod() && true === $form->isValid()) {
            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.created');
            $userToken = $repo->create($form->getData(), $this->getUser());

            return $this->redirectToRoute('ci_app_user_token_show', ['userToken' => $userToken->getId()]);
        }

        return $this->render('CIAppBundle:UserToken:create.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    public function editAction(Request $request, UserToken $userToken)
    {
        $form = $this->createForm(UserTokenType::class, $userToken);

        $form->handleRequest($request);

        if ('POST' === $request->getMethod() && true === $form->isValid()) {
            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.edited');
            $this->getDoctrine()->getRepository(UserToken::class)->edit($form->getData(), $this->getUser());

            return $this->redirectToRoute('ci_app_user_token_show', ['userToken' => $userToken->getId()]);
        }

        return $this->render('CIAppBundle:UserToken:edit.html.twig', [
            'form' => $form->createView(),
            'userToken' => $userToken,
        ]);
    }

    public function deleteAction(UserToken $userToken)
    {
        try {
            $this->getDoctrine()->getRepository(UserToken::class)->delete($userToken, $this->getUser());

            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.delete');

        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $this->get('translator')->trans($e->getMessage()));
        }

        return $this->redirectToRoute('ci_app_user_token_index');
    }
}
