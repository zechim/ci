<?php

namespace CI\AppBundle\Controller;

use CI\AppBundle\Entity\History;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HistoryController extends Controller
{
    public function showAction(History $history)
    {
        return $this->render('CIAppBundle:History:show.html.twig', [
            'history' => $history,
            'response' => var_export($history->getQueue()->getResponse(), true)
        ]);
    }
}
