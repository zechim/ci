<?php

namespace CI\AppBundle\Controller;

use CI\AppBundle\Entity\UserPermission;
use CI\AppBundle\Form\UserPermissionType;
use CI\AppBundle\Repository\UserPermissionRepository;
use CI\AppBundle\Service\Pagination\Pagination;
use CI\AppBundle\Twig\MessageExtension;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserPermissionController extends Controller
{
    public function indexAction($page)
    {
        $pagination = $this->getDoctrine()
            ->getManager()
            ->getRepository('CIAppBundle:UserPermission')
            ->getPagination($page);

        $pagination->set(Pagination::ROUTE, 'ci_app_user_permission_index');

        return $this->render('CIAppBundle:UserPermission:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    public function showAction(UserPermission $userPermission)
    {
        return $this->render('CIAppBundle:UserPermission:show.html.twig', [
            'userPermission' => $userPermission
        ]);
    }

    public function createAction(Request $request)
    {
        /** @var UserPermissionRepository $repo */
        $repo = $this->getDoctrine()->getRepository(UserPermission::class);

        $form = $this->createForm(UserPermissionType::class);

        $form->handleRequest($request);

        if ('POST' === $request->getMethod() && true === $form->isValid()) {
            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.created');
            $userPermission = $repo->create($form->getData(), $this->getUser());

            return $this->redirectToRoute('ci_app_user_permission_show', ['userPermission' => $userPermission->getId()]);
        }

        return $this->render('CIAppBundle:UserPermission:create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request, UserPermission $userPermission)
    {
        if (false === $userPermission->getEditable() || true === $userPermission->getAdmin()) {
            return $this->redirectToRoute('ci_app_user_permission_index');
        }

        $form = $this->createForm(UserPermissionType::class, $userPermission);

        $form->handleRequest($request);

        if ('POST' === $request->getMethod() && true === $form->isValid()) {
            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.edited');
            $this->getDoctrine()->getRepository(UserPermission::class)->edit($form->getData(), $this->getUser());

            return $this->redirectToRoute('ci_app_user_permission_show', ['userPermission' => $userPermission->getId()]);
        }

        return $this->render('CIAppBundle:UserPermission:edit.html.twig', [
            'form' => $form->createView(),
            'userPermission' => $userPermission,
        ]);
    }

    public function deleteAction(UserPermission $userPermission)
    {
        if (false === $userPermission->getEditable() || true === $userPermission->getAdmin()) {
            return $this->redirectToRoute('ci_app_user_permission_index');
        }

        try {
            $this->getDoctrine()->getRepository(UserPermission::class)->delete($userPermission, $this->getUser());

            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.delete');

        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $this->get('translator')->trans($e->getMessage()));
        }

        return $this->redirectToRoute('ci_app_user_permission_index');
    }
}
