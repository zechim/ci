<?php

namespace CI\AppBundle\Controller;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\ServerConfig;
use CI\AppBundle\Entity\ServerUpload;
use CI\AppBundle\Form\ServerType;
use CI\AppBundle\Repository\ServerRepository;
use CI\AppBundle\Service\Pagination\Pagination;
use CI\AppBundle\Twig\MessageExtension;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ServerController extends Controller
{
    public function indexAction($page)
    {
        $pagination = $this->getDoctrine()
            ->getManager()
            ->getRepository('CIAppBundle:Server')
            ->getPagination($page);

        $pagination->set(Pagination::ROUTE, 'ci_app_server_index');

        return $this->render('CIAppBundle:Server:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    public function showAction(Server $server)
    {
        return $this->render('CIAppBundle:Server:show.html.twig', [
            'server' => $server
        ]);
    }

    public function createAction(Request $request, Application $application = null)
    {
        $api = $request->get('api');

        /** @var ServerRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Server::class);

        $form = $this->createForm(
            ServerType::class,
            $repo->getDefault($application),
            [
                'serverConfigRepo' => $this->getDoctrine()->getRepository(ServerConfig::class),
                'translator' => $this->get('translator'),
                'csrf_protection' => false === $api,
            ]
        );

        $form->handleRequest($request);

        if (true === $form->isSubmitted()) {
            if (true === $form->isValid()) {
                $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.created');
                $server = $repo->create($form->getData(), $this->getUser());

                if (true === $api) {
                    return new JsonResponse([
                        'error' => false,
                        'server' => $server->getId(),
                        'message' => $this->get('translator')->trans('trans.message.created')
                    ]);
                }

                return $this->redirectToRoute('ci_app_server_show', ['server' => $server->getId()]);
            }

            if (true === $api) {
                return new JsonResponse([
                    'error' => true,
                    'errors' => $this->get('ci.form.explorer')->errorToArray($form)
                ]);
            }
        }

        return $this->render('CIAppBundle:Server:create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request, Server $server)
    {
        $api = $request->get('api');

        $form = $this->createForm(
            ServerType::class,
            $server,
            [
                'serverConfigRepo' => $this->getDoctrine()->getRepository(ServerConfig::class),
                'translator' => $this->get('translator'),
                'csrf_protection' => false === $api,
            ]
        );

        $form->handleRequest($request);

        if (true === $form->isSubmitted()) {
            if (true === $form->isValid()) {
                $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.edited');
                $this->getDoctrine()->getRepository(Server::class)->edit($form->getData(), $this->getUser());

                if (true === $api) {
                    return new JsonResponse([
                        'error' => false,
                        'message' => $this->get('translator')->trans('trans.message.edited')
                    ]);
                }

                return $this->redirectToRoute('ci_app_server_show', ['server' => $server->getId()]);
            }

            if (true === $api) {
                return new JsonResponse([
                    'error' => true,
                    'errors' => $this->get('ci.form.explorer')->errorToArray($form)
                ]);
            }
        }

        return $this->render('CIAppBundle:Server:edit.html.twig', [
            'form' => $form->createView(),
            'server' => $server,
        ]);
    }

    public function showUploadAction(ServerUpload $serverUpload)
    {
        return $this->render('CIAppBundle:Server:show.server.upload.html.twig', [
            'serverUpload' => $serverUpload,
        ]);
    }

    public function deployAction(Request $request, Server $server)
    {
        $api = $request->get('api');

        try {
            $this->getDoctrine()->getRepository(Server::class)->dump($server, $this->getUser(), true);

            $message = 'ok';
            $error = false;

        } catch (\Exception $e) {
            $message = $e->getMessage();
            $error = true;

            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $message);
        }

        if (true === $api) {
            return new JsonResponse([
                'error' => $error,
                'message' => $message
            ]);
        }

        return $this->redirectToRoute('ci_app_server_show', ['server' => $server->getId()]);
    }

    public function deleteAction(Request $request, Server $server)
    {
        $error = false;
        $message = null;

        try {
            $this->getDoctrine()->getRepository(Server::class)->delete($server);

            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, $message = 'trans.message.deleted');

        } catch (\Exception $e) {
            $error = true;

            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $message = $e->getMessage());
        }

        if (true === $request->get('api')) {
            return new JsonResponse([
                'error' => $error,
                'message' => $this->get('translator')->trans($message)
            ]);
        }

        return $this->redirectToRoute('ci_app_server_index');
    }
}
