<?php

namespace CI\AppBundle\Controller;

use CI\AppBundle\Entity\Tag;
use CI\AppBundle\Twig\MessageExtension;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TagController extends Controller
{
    public function deleteAction(Tag $tag)
    {
        try {
            $this->getDoctrine()->getRepository(Tag::class)->delete($tag, $this->getUser());

        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $this->get('translator')->trans($e->getMessage()));
        }

        return $this->redirectToRoute('ci_app_build_show', ['build' => $tag->getBuild()->getId()]);
    }

    public function buildAction(Tag $tag)
    {
        try {
            $this->getDoctrine()->getRepository(Tag::class)->build($tag, $this->getUser());

        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $e->getMessage());
        }

        return $this->redirectToRoute('ci_app_build_show', ['build' => $tag->getBuild()->getId()]);
    }
}
