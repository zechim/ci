<?php

namespace CI\AppBundle\Controller;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\Hook;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Form\HookType;
use CI\AppBundle\Repository\HookRepository;
use CI\AppBundle\Service\Pagination\Pagination;
use CI\AppBundle\Twig\MessageExtension;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class HookController extends Controller
{
    public function indexAction($page)
    {
        $pagination = $this->getDoctrine()
            ->getManager()
            ->getRepository('CIAppBundle:Hook')
            ->getPagination($page);

        $pagination->set(Pagination::ROUTE, 'ci_app_hook_index');

        return $this->render('CIAppBundle:Hook:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    public function showAction(Hook $hook)
    {
        return $this->render('CIAppBundle:Hook:show.html.twig', [
            'hook' => $hook
        ]);
    }

    public function createAction(Request $request)
    {
        /** @var HookRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Hook::class);

        $form = $this->createForm(HookType::class, $repo->getDefault());
        $form->handleRequest($request);

        if ('POST' === $request->getMethod() && true === $form->isValid()) {
            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.created');
            $hook = $repo->create($form->getData(), $this->getUser());

            return $this->redirectToRoute('ci_app_hook_show', ['hook' => $hook->getId()]);
        }

        return $this->render('CIAppBundle:Hook:create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request, Hook $hook)
    {
        $form = $this->createForm(HookType::class, $hook);
        $form->handleRequest($request);

        if ('POST' === $request->getMethod() && true === $form->isValid()) {
            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.edited');
            $this->getDoctrine()->getRepository(Hook::class)->edit($form->getData(), $this->getUser());

            return $this->redirectToRoute('ci_app_hook_show', ['hook' => $hook->getId()]);
        }

        return $this->render('CIAppBundle:Hook:edit.html.twig', [
            'form' => $form->createView(),
            'hook' => $hook,
        ]);
    }

    public function deleteAction(Request $request, Hook $hook)
    {
        $error = false;
        $message = null;

        try {
            $this->getDoctrine()->getRepository(Hook::class)->delete($hook);

            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, $message = 'trans.message.deleted');

        } catch (\Exception $e) {
            $error = true;

            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $message = $e->getMessage());
        }

        if (true === $request->get('api')) {
            return new JsonResponse([
                'error' => $error,
                'message' => $this->get('translator')->trans($message)
            ]);
        }

        return $this->redirectToRoute('ci_app_hook_index');
    }

    public function runAction(Request $request, Server $server, Hook $hook)
    {
        $error = false;
        $message = null;

        try {
            if ($this->getDoctrine()->getRepository(Server::class)->runHook($server, $hook, $this->getUser())) {
                $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, $message = 'trans.message.created');

            } else {
                $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, $message = 'trans.message.not.created');
            }


        } catch (\Exception $e) {
            $error = true;

            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $message = $e->getMessage());
        }

        if (true === $request->get('api')) {
            return new JsonResponse([
                'error' => $error,
                'message' => $this->get('translator')->trans($message)
            ]);
        }

        return $this->redirectToRoute('ci_app_server_show', ['server' => $server->getId()]);
    }
}
