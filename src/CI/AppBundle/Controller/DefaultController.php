<?php

namespace CI\AppBundle\Controller;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\History;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Service\Pagination\Pagination;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('CIAppBundle:Default:index.html.twig', [
            'count' => [
                'application' => $em->getRepository(Application::class)->getTotal(),
                'server' => $em->getRepository(Server::class)->getTotal(),
            ],
            'last' => [
                'history' => $em->getRepository(History::class)->getPagination(1, 5)->get(Pagination::RESULT),
            ]
        ]);
    }
}
