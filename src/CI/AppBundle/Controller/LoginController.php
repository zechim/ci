<?php

namespace CI\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LoginController extends Controller
{
    public function indexAction()
    {
        return $this->redirectToRoute('ci_app_index');
    }

    public function loginAction()
    {
        $helper = $this->get('security.authentication_utils');

        return $this->render('CIAppBundle::login.html.twig', [
            'last_username' => $helper->getLastUsername(),
            'error' => $helper->getLastAuthenticationError(),
        ]);
    }
}
