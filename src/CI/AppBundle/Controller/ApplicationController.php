<?php

namespace CI\AppBundle\Controller;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\ApplicationConfig;
use CI\AppBundle\Entity\ApplicationUpload;
use CI\AppBundle\Form\ApplicationType;
use CI\AppBundle\Repository\ApplicationRepository;
use CI\AppBundle\Service\Pagination\Pagination;
use CI\AppBundle\Twig\MessageExtension;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApplicationController extends Controller
{
    public function indexAction($page)
    {
        $pagination = $this->getDoctrine()
            ->getManager()
            ->getRepository('CIAppBundle:Application')
            ->getPagination($page);

        $pagination->set(Pagination::ROUTE, 'ci_app_application_index');

        return $this->render('CIAppBundle:Application:index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    public function showAction(Application $application)
    {
        return $this->render('CIAppBundle:Application:show.html.twig', [
            'application' => $application
        ]);
    }

    public function createAction(Request $request)
    {
        $api = $request->get('api');

        /** @var ApplicationRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Application::class);

        $form = $this->createForm(
            ApplicationType::class,
            $repo->getDefault(),
            [
                'applicationConfigRepo' => $this->getDoctrine()->getRepository(ApplicationConfig::class),
                'translator' => $this->get('translator'),
                'csrf_protection' => false === $api,
            ]
        );

        $form->handleRequest($request);

        if (true === $form->isSubmitted()) {
            if (true === $form->isValid()) {
                $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.created');
                $application = $repo->create($form->getData(), $this->getUser());

                if (true === $api) {
                    return new JsonResponse([
                        'error' => false,
                        'application' => $application->getId(),
                        'message' => $this->get('translator')->trans('trans.message.created')
                    ]);
                }

                return $this->redirectToRoute('ci_app_application_show', ['application' => $application->getId()]);
            }

            if (true === $api) {
                return new JsonResponse([
                    'error' => true,
                    'errors' => $this->get('ci.form.explorer')->errorToArray($form)
                ]);
            }
        }

        return $this->render('CIAppBundle:Application:create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request, Application $application)
    {
        $api = $request->get('api');

        $form = $this->createForm(
            ApplicationType::class,
            $application,
            [
                'applicationConfigRepo' => $this->getDoctrine()->getRepository(ApplicationConfig::class),
                'translator' => $this->get('translator'),
                'csrf_protection' => false === $api,
            ]
        );

        $form->handleRequest($request);

        if (true === $form->isSubmitted()) {
            if (true === $form->isValid()) {
                $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, 'trans.message.edited');
                $this->getDoctrine()->getRepository(Application::class)->edit($form->getData(), $this->getUser());

                if (true === $api) {
                    return new JsonResponse([
                        'error' => false,
                        'message' => $this->get('translator')->trans('trans.message.edited')
                    ]);
                }

                return $this->redirectToRoute('ci_app_application_show', ['application' => $application->getId()]);
            }

            if (true === $api) {
                return new JsonResponse([
                    'error' => true,
                    'errors' => $this->get('ci.form.explorer')->errorToArray($form)
                ]);
            }
        }

        return $this->render('CIAppBundle:Application:edit.html.twig', [
            'form' => $form->createView(),
            'application' => $application,
        ]);
    }

    public function showUploadAction(ApplicationUpload $applicationUpload)
    {
        return $this->render('CIAppBundle:Application:show.application.upload.html.twig', [
            'applicationUpload' => $applicationUpload,
        ]);
    }

    public function deployAction(Request $request, Application $application)
    {
        $api = $request->get('api');

        try {
            $this->getDoctrine()->getRepository(Application::class)->dump($application, $this->getUser(), true);

            $message = 'ok';
            $error = false;

        } catch (\Exception $e) {
            $message = $e->getMessage();
            $error = true;

            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $message);
        }

        if (true === $api) {
            return new JsonResponse([
                'error' => $error,
                'message' => $message
            ]);
        }

        return $this->redirectToRoute('ci_app_application_show', ['application' => $application->getId()]);
    }

    public function deleteAction(Request $request, Application $application)
    {
        $error = false;
        $message = null;

        try {
            $this->getDoctrine()->getRepository(Application::class)->delete($application);

            $this->get('session')->getFlashBag()->add(MessageExtension::SUCCESS, $message = 'trans.message.deleted');

        } catch (\Exception $e) {
            $error = true;

            $this->get('session')->getFlashBag()->add(MessageExtension::ERROR, $message = $e->getMessage());
        }

        if (true === $request->get('api')) {
            return new JsonResponse([
                'error' => $error,
                'message' => $this->get('translator')->trans($message)
            ]);
        }

        return $this->redirectToRoute('ci_app_application_index');
    }
}
