<?php

namespace CI\AppBundle\Service\Deploy\Structure;

use CI\AppBundle\Entity\Server;

class Capfile
{
    /**
     * @var StructureInterface
     */
    protected $structure;

    /**
     * Capfile constructor.
     * @param StructureInterface $structure
     */
    public function __construct(StructureInterface $structure)
    {
        $this->structure = $structure;
    }

    public function dump(Server $server)
    {
        $capfile = $this->structure->getCapfilePath($server->getApplication());

        if (
            true === $this->structure->isFileOutdated($capfile, $server->getUpdatedAt()) ||
            true === $this->structure->isFileOutdated($capfile, $server->getApplication()->getUpdatedAt())
        ) {
            $this->structure->removeFile($capfile);
        }

        if (true === $this->structure->hasFile($capfile)) {
            return;
        }

        $this->structure->dumpCapfile($capfile, []);
    }
}
