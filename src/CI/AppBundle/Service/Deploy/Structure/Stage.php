<?php

namespace CI\AppBundle\Service\Deploy\Structure;

use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\Type;

class Stage
{
    /**
     * @var StructureInterface
     */
    protected $structure;

    /**
     * Capfile constructor.
     * @param StructureInterface $structure
     */
    public function __construct(StructureInterface $structure)
    {
        $this->structure = $structure;
    }

    /**
     * @param Server $server
     */
    public function dump(Server $server)
    {
        $stage = $this->structure->getStagePath($server);

        if (
            true === $this->structure->isFileOutdated($stage, $server->getUpdatedAt()) ||
            true === $this->structure->isFileOutdated($stage, $server->getApplication()->getUpdatedAt())
        ) {
            $this->structure->removeFile($stage);
        }

        if (true === $this->structure->hasFile($stage)) {
            return;
        }

        $this->structure->dumpStage($stage, [
            'server' => $server,
            'user' => $server->getServerConfig(Type::CODE_CONFIG_USER),
            'ip' => $server->getServerConfig(Type::CODE_CONFIG_IP),
            'key' => $this->getKey($server),
            'branch' => $server->getServerConfig(Type::CODE_CONFIG_BRANCH),
            'deploy_to' => $server->getServerConfig(Type::CODE_CONFIG_DEPLOY_TO),
            'hooks' => $server->getServerHook(true),
            'uploads' => $server->getServerUpload(true)
        ]);
    }

    /**
     * @param Server $server
     * @return string
     */
    public function getKey(Server $server)
    {
        $key = $this->structure->getKeyPath($server);

        if (true === $this->structure->isFileOutdated($key, $server->getUpdatedAt())) {
            $this->structure->removeFile($key);
        }

        if (false === $this->structure->hasFile($key)) {
            $this->structure->dumpKey($key, $server->getServerConfig(Type::CODE_CONFIG_KEY));
        }

        return $key;
    }
}
