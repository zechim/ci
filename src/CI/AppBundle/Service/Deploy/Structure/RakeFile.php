<?php

namespace CI\AppBundle\Service\Deploy\Structure;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\ApplicationUpload;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\ServerUpload;
use CI\AppBundle\Entity\Type;
use Doctrine\Common\Collections\Collection;

class RakeFile
{
    /**
     * @var StructureInterface
     */
    protected $structure;

    /**
     * Capfile constructor.
     * @param StructureInterface $structure
     */
    public function __construct(StructureInterface $structure)
    {
        $this->structure = $structure;
    }

    public function dump(Server $server)
    {
        $uploads = $server->getServerUpload(true);
        $uploadPath = $this->structure->getUploadPath($server);

        $this->dumpUpload($uploads, $uploadPath);

        $this->dumpRake(
            $server,
            sprintf(
                '%sserver_%s.rake',
                $this->structure->getRakePath($server->getApplication()),
                $server->getId()
            ),
            $this->structure->getRakeFilesTemplate(),
            [
                'hooks' => $server->getServerHook(true),
                'uploads' => $uploads,
                'upload_path' => $uploadPath
            ]
        );
    }

    /**
     * @param Collection $uploads
     * @param $uploadPath
     */
    public function dumpUpload(Collection $uploads, $uploadPath)
    {
        /** @var ApplicationUpload|ServerUpload $upload */
        foreach ($uploads as $upload) {
            $file = $uploadPath . $upload->getUploadName();

            if (
                true === $this->structure->isFileOutdated($file, $upload->getUpdatedAt())
            ) {
                $this->structure->removeFile($file);
            }

            if (true === $this->structure->hasFile($file)) {
                continue;
            }

            $this->structure->dumpUpload($file, $upload->getValue());
        }
    }

    /**
     * @param Server $server
     * @param string $rake
     * @param string $template
     * @param array $parameters
     */
    public function dumpRake(Server $server, $rake, $template, $parameters)
    {
        if (
            true === $this->structure->isFileOutdated($rake, $server->getUpdatedAt()) ||
            true === $this->structure->isFileOutdated($rake, $server->getApplication()->getUpdatedAt())
        ) {
            $this->structure->removeFile($rake);
        }

        if (true === $this->structure->hasFile($rake)) {
            return;
        }

        $parameters['server'] = $server;
        $parameters['branch'] = $server->getServerConfig(Type::CODE_CONFIG_BRANCH);

        $this->structure->dumpRake($rake, $template, $parameters);
    }
}
