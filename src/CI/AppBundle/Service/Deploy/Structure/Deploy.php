<?php

namespace CI\AppBundle\Service\Deploy\Structure;

use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\Type;

class Deploy
{
    /**
     * @var StructureInterface
     */
    protected $structure;

    /**
     * Capfile constructor.
     * @param StructureInterface $structure
     */
    public function __construct(StructureInterface $structure)
    {
        $this->structure = $structure;
    }

    public function dump(Server $server)
    {
        $deploy = $this->structure->getDeployPath($server->getApplication());

        if (
            true === $this->structure->isFileOutdated($deploy, $server->getUpdatedAt()) ||
            true === $this->structure->isFileOutdated($deploy, $server->getApplication()->getUpdatedAt())
        ) {
            $this->structure->removeFile($deploy);
        }

        if (true === $this->structure->hasFile($deploy)) {
            return;
        }

        $this->structure->dumpDeploy($deploy, [
            'application' => preg_replace("/[^0-9a-zA-Z]/m", '', $server->getApplication()->getName()),

            // one repo per application
            'repo' => $server->getApplication()->getApplicationConfig(Type::CODE_CONFIG_REPO)
        ]);
    }
}
