<?php

namespace CI\AppBundle\Service\Deploy\Structure;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\Server;

interface StructureInterface
{
    /**
     * @param Application $application
     * @return void
     */
    function create(Application $application);

    /**
     * @param $filename
     * @return void
     */
    function removeFile($filename);

    /**
     * @param $filename
     * @param \DateTime $compare
     * @return bool
     */
    function isFileOutdated($filename, \DateTime $compare);

    /**
     * @param $filename
     * @return bool
     */
    function hasFile($filename);

    /**
     * @param Application $application
     * @return string
     */
    function getBasePath(Application $application);

    /**
     * @param Application $application
     * @return string
     */
    function getCapfilePath(Application $application);

    /**
     * @param Application $application
     * @return string
     */
    function getDeployPath(Application $application);

    /**
     * @param Server $server
     * @return mixed
     */
    function getStagePath(Server $server);

    /**
     * @param Application $application
     * @return mixed
     */
    function getRakePath(Application $application);

    /**
     * @param Server $server
     * @return string
     */
    function getKeyPath(Server $server);

    /**
     * @param Server $server
     * @return mixed
     */
    function getUploadPath(Server $server);

    /**
     * @return array
     */
    function getRakeFilesTemplate();

    /**
     * @param string $to
     * @param array $params
     * @return void
     */
    function dumpCapfile($to, array $params);

    /**
     * @param $to
     * @param array $params
     * @return void
     */
    function dumpDeploy($to, array $params);

    /**
     * @param $to
     * @param array $params
     * @return void
     */
    function dumpStage($to, array $params);

    /**
     * @param $to
     * @param string $content
     * @return void
     */
    function dumpKey($to, $content);

    /**
     * @param $to
     * @param string $content
     * @return void
     */
    function dumpUpload($to, $content);

    /**
     * @param $to
     * @param $template
     * @param array $params
     * @return void
     */
    function dumpRake($to, $template, array $params);
}
