<?php

namespace CI\AppBundle\Service\Deploy\Structure;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\Type;
use CI\AppBundle\Service\Deploy\ConfigBagInterface;
use Symfony\Component\Filesystem\Filesystem;

class Structure implements StructureInterface
{
    /**
     * @var ConfigBagInterface
     */
    protected $config;

    /**
     * ApplicationStructure constructor.
     * @param ConfigBagInterface $config
     */
    public function __construct(ConfigBagInterface $config)
    {
        $this->config = $config;
    }

    /**
     * @param Application $application
     * @retun void
     */
    public function create(Application $application)
    {
        (new Filesystem())->mkdir([$this->config->getBasePath($application)]);
    }

    /**
     * @param $filename
     */
    public function removeFile($filename)
    {
        if (true === is_file($filename)) {
            unlink($filename);
        }
    }

    /**
     * @param $filename
     * @param \DateTime $compare
     * @return bool
     */
    public function isFileOutdated($filename, \DateTime $compare)
    {
        if (false === $this->hasFile($filename)) {
            return false;
        }

        $stat = stat($filename);

        return $compare > (new \DateTime())->setTimestamp($stat['mtime']);
    }

    /**
     * @param $filename
     * @return bool
     */
    public function hasFile($filename)
    {
        return true === is_file($filename);
    }

    /**
     * @param Application $application
     * @return string
     */
    public function getBasePath(Application $application)
    {
        return $this->config->getBasePath($application);
    }


    /**
     * @param Application $application
     * @return string
     */
    public function getCapfilePath(Application $application)
    {
        return $this->config->getCapfilePath($application) . 'Capfile';
    }

    /**
     * @param Application $application
     * @return string
     */
    public function getDeployPath(Application $application)
    {
        return $this->config->getDeployPath($application) . 'deploy.rb';
    }

    /**
     * @param Server $server
     * @return string
     */
    public function getStagePath(Server $server)
    {
        return $this->config->getStagePath($server) . $server->getId() . '.rb';
    }

    /**
     * @param Application $application
     * @return mixed
     */
    public function getRakePath(Application $application)
    {
        return $this->config->getRakePath($application);
    }

    /**
     * @param Server $server
     * @return string
     */
    public function getKeyPath(Server $server)
    {
        return $this->config->getKeyPath($server) . $server ->getId() . '.pem';
    }

    /**
     * @param Server $server
     * @return string
     */
    public function getUploadPath(Server $server)
    {
        return $this->config->getUploadPath($server);
    }

    /**
     * @return array
     */
    public function getRakeFilesTemplate()
    {
        return $this->config->getRakeFilesTemplate();
    }

    /**
     * @param string $to
     * @param array $params
     */
    public function dumpCapfile($to, array $params)
    {
        (new Filesystem())->dumpFile(
            $to,
            $this->config->getTemplating()->render($this->config->getCapfileTemplate(), $params)
        );
    }

    /**
     * @param string $to
     * @param array $params
     */
    public function dumpDeploy($to, array $params)
    {
        (new Filesystem())->dumpFile(
            $to,
            $this->config->getTemplating()->render($this->config->getDeployTemplate(), $params)
        );
    }

    /**
     * @param string $to
     * @param array $params
     */
    public function dumpStage($to, array $params)
    {
        (new Filesystem())->dumpFile(
            $to,
            $this->config->getTemplating()->render($this->config->getStageTemplate(), $params)
        );
    }

    /**
     * @param $to
     * @param string $content
     */
    public function dumpUpload($to, $content)
    {
        (new Filesystem())->dumpFile(
            $to,
            $content
        );
    }

    /**
     * @param $to
     * @param string $template
     * @param array $params
     */
    public function dumpRake($to, $template, array $params)
    {
        (new Filesystem())->dumpFile(
            $to,
            $this->config->getTemplating()->render($template, $params)
        );
    }

    /**
     * @param $to
     * @param string $content
     */
    public function dumpKey($to, $content)
    {
        (new Filesystem())->dumpFile($to, $content);
    }
}
