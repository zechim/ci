<?php

namespace CI\AppBundle\Service\Deploy;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\Build;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Service\Deploy\Structure\Structure;
use CI\AppBundle\Service\Deploy\Structure\StructureInterface;
use Symfony\Component\Templating\EngineInterface;

class ConfigBag implements ConfigBagInterface
{
    const BASE_PATH = 'base_path';
    const TEMPLATE_CAPFILE = 'template_capfile';
    const TEMPLATE_DEPLOY = 'template_deploy';
    const TEMPLATE_STAGE = 'template_stage';
    const TEMPLATE_RAKE_FILE = 'template_rake_file';

    /**
     * @var array
     */
    protected $parameters = [];

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var StructureInterface
     */
    protected $structure;

    /**
     * ConfigBag constructor.
     * @param EngineInterface $templating
     * @param array $parameters
     */
    public function __construct(
        EngineInterface $templating,
        array $parameters)
    {
        $this->templating = $templating;
        $this->parameters = $parameters;
    }

    /**
     * @param Build $build
     * @return string
     */
    public function getBuildPath(Build $build)
    {
        return $this->parameters[self::BASE_PATH] . 'build' . DIRECTORY_SEPARATOR . $build->getId() . DIRECTORY_SEPARATOR;
    }

    /**
     * @param Application $application
     * @return string
     */
    public function getBasePath(Application $application)
    {
        return $this->parameters[self::BASE_PATH] . $application->getId() . DIRECTORY_SEPARATOR;
    }

    /**
     * @param Application $application
     * @return string
     */
    public function getCapfilePath(Application $application)
    {
        return $this->getBasePath($application);
    }

    /**
     * @param Application $application
     * @return string
     */
    public function getDeployPath(Application $application)
    {
        return $this->getConfigPath($application);
    }

    /**
     * @param Application $application
     * @return string
     */
    public function getConfigPath(Application $application)
    {
        return $this->getBasePath($application) . 'config' . DIRECTORY_SEPARATOR;
    }

    /**
     * @param Server $server
     * @return string
     */
    public function getStagePath(Server $server)
    {
        return $this->getConfigPath($server->getApplication()) . 'deploy' . DIRECTORY_SEPARATOR;
    }

    /**
     * @param Application $application
     * @return string
     */
    public function getRakePath(Application $application)
    {
        return $this->getBasePath($application) . 'lib' . DIRECTORY_SEPARATOR . 'task' . DIRECTORY_SEPARATOR;
    }

    /**
     * @param Server $server
     * @return string
     */
    public function getKeyPath(Server $server)
    {
        return $this->getBasePath($server->getApplication()) . 'key' . DIRECTORY_SEPARATOR;
    }

    /**
     * @param Server $server
     * @return string
     */
    public function getUploadPath(Server $server)
    {
        return $this->getBasePath($server->getApplication()) . 'upload' . DIRECTORY_SEPARATOR;
    }

    /**
     * @return string
     */
    public function getCapfileTemplate()
    {
        return $this->parameters[self::TEMPLATE_CAPFILE];
    }

    /**
     * @return string
     */
    public function getDeployTemplate()
    {
        return $this->parameters[self::TEMPLATE_DEPLOY];
    }

    /**
     * @return string
     */
    public function getStageTemplate()
    {
        return $this->parameters[self::TEMPLATE_STAGE];
    }

    /**
     * @return array
     */
    public function getRakeFilesTemplate()
    {
        return $this->parameters[self::TEMPLATE_RAKE_FILE];
    }

    /**
     * @return \Symfony\Component\Templating\EngineInterface
     */
    public function getTemplating()
    {
        return $this->templating;
    }

    /**
     * @return StructureInterface
     */
    public function getStructure()
    {
        if (null === $this->structure) {
            $this->structure = new Structure($this);
        }

        return $this->structure;
    }
}
