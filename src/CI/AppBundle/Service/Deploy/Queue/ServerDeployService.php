<?php

namespace CI\AppBundle\Service\Deploy\Queue;

use CI\AppBundle\Entity\Queue;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\User;
use CI\AppBundle\Repository\ServerRepository;
use CI\AppBundle\Service\Deploy\Queue\Event\DeployEvent;
use CI\AppBundle\Service\Queue\AbstractService;
use CI\AppBundle\Service\Queue\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Process\Process;

class ServerDeployService extends AbstractService
{
    public function dispatch(Queue $queue, Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        /** @var ServerRepository $serverRepo */
        $serverRepo = $em->getRepository(Server::class);

        /** @var Server $server */
        $server = $serverRepo->find($request->get('server'));
        $user = $this->container->get('doctrine.orm.entity_manager')->getRepository(User::class)->find($request->get('user'));

        $serverRepo->setDeploying($server, $user);

        $process = new Process(sprintf('cap %s deploy', $server->getId()));
        $process->setWorkingDirectory($this->container->get('ci_app.structure')->getBasePath($server->getApplication()));
        $process->setTimeout(0);
        $process->start();
        $process->wait(function ($type, $buffer) use ($em, $server, $queue, &$response) {
            $output = $queue->getResponse();
            $output = true === is_array($output) && true === array_key_exists('output', $output) ? $output['output'] : '';

            $queue->setResponse(
                [
                    'gitlog' => ['commit' => '', 'commiter' => '', 'date' => '', 'message' => ''],
                    'status' => (string) $server->getStatusType(),
                    'output' => $output . $buffer
                ]
            );

            $em->persist($queue);
            $em->flush($queue);
        });

        $response = $queue->getResponse();

        $this->response->setSuccess(0 === preg_match('/DEPLOY FAILED/', $queue->getResponse()['output']));

        if (true === $this->response->getSuccess()) {
            $response['gitlog'] = $this->getGitLog($queue->getResponse()['output']);
        }

        $this->response->setResponse($response);

        $response = clone $this->response;
        $response->setResponse($response->getResponse() + ['server' => $server]);
        $this->container->get('event_dispatcher')->dispatch(DeployEvent::NAME, new DeployEvent($response));

        $serverRepo->setDeployed($server, $user);
        $serverRepo->setReady($server, $user);
    }

    public function getGitLog($output)
    {
        preg_match('/\sgitlog:[^\r\n]*/m', $output, $match);

        $log = ['commit' => '', 'commiter' => '', 'date' => '', 'message' => ''];

        if (false === is_array($match)) {
            return $log;
        }

        if (1 !== count($match)) {
            return $log;
        }

        $match = trim(str_replace('gitlog:', '', $match[0]));
        $match = explode(';', $match);

        if (4 > count($match)) {
            return $log;
        }

        $log['commit'] = array_shift($match);
        $log['commiter'] = array_shift($match);
        $log['date'] = array_shift($match);
        $log['message'] = implode(';', $match);

        return $log;
    }

    public static function createRequest(Server $server, User $user)
    {
        return [
            'server' => $server->getId(),
            'user' => $user->getId(),
        ];
    }
}
