<?php

namespace CI\AppBundle\Service\Deploy\Queue;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\Queue;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\User;
use CI\AppBundle\Repository\ServerRepository;
use CI\AppBundle\Service\Deploy\Structure\Structure;
use CI\AppBundle\Service\Deploy\Structure\StructureInterface;
use CI\AppBundle\Service\Queue\AbstractService;
use CI\AppBundle\Service\Queue\Request;
use Symfony\Component\Templating\EngineInterface;

class ServerDumperService extends AbstractService
{
    public function dispatch(Queue $queue, Request $request)
    {
        /** @var ServerRepository $serverRepo */
        $serverRepo = $this->container->get('doctrine.orm.entity_manager')->getRepository(Server::class);

        /** @var Server $server */
        $server = $serverRepo->find($request->get('server'));

        /** @var User $user */
        $user = $this->container->get('doctrine.orm.entity_manager')->getRepository(User::class)->find($request->get('user'));

        $serverRepo->setDumping($server, $user);

        $this->container->get('ci_app.dumper')->dumpServer($server);

        $serverRepo->setDumped($server, $user);
        $serverRepo->setReady($server, $user);

        $this->response->setResponse([
            'status' => (string) $server->getStatusType(),
            'output' => '',
        ]);

        $queue->setResponse($this->response->getResponse());

        $this->response->setSuccess(true);

        if (true === $request->get('deploy')) {
            $serverRepo->deploy($server, $user);
        }
    }

    /**
     * @param Server $server
     * @param User $user
     * @param bool $deploy
     * @return array
     */
    public static function createRequest(Server $server, User $user, $deploy = true)
    {
        return [
            'server' => $server->getId(),
            'user' => $user->getId(),
            'deploy' => (boolean) $deploy
        ];
    }
}
