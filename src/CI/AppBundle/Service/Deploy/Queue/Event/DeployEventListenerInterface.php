<?php

namespace CI\AppBundle\Service\Deploy\Queue\Event;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;

interface DeployEventListenerInterface extends ContainerAwareInterface
{
    /**
     * @param array $parameters
     */
    public function setParameters(array $parameters);

    /**
     * @param DeployEvent $event
     */
    public function onDeployEvent(DeployEvent $event);
}
