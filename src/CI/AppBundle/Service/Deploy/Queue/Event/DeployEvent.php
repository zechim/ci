<?php

namespace CI\AppBundle\Service\Deploy\Queue\Event;

use CI\AppBundle\Entity\Queue;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\User;
use CI\AppBundle\Repository\QueueRepository;
use CI\AppBundle\Repository\ServerRepository;
use CI\AppBundle\Service\Queue\AbstractService;
use CI\AppBundle\Service\Queue\Request;
use CI\AppBundle\Service\Queue\Response;
use CI\AppBundle\Service\WebHook\Slack;
use CI\AppBundle\Service\WebHook\SlackService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Process\Process;

class DeployEvent extends Event
{
    const NAME = 'ci.deploy.event';

    /**
     * @var Response
     */
    protected $response;

    /**
     * DeployEvent constructor.
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }
}
