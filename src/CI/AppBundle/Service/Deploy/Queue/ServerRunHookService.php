<?php

namespace CI\AppBundle\Service\Deploy\Queue;

use CI\AppBundle\Entity\Hook;
use CI\AppBundle\Entity\Queue;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\User;
use CI\AppBundle\Repository\ServerRepository;
use CI\AppBundle\Service\Queue\AbstractService;
use CI\AppBundle\Service\Queue\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Process\Process;

class ServerRunHookService extends AbstractService
{
    public function dispatch(Queue $queue, Request $request)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        /** @var ServerRepository $serverRepo */
        $serverRepo = $em->getRepository(Server::class);

        /** @var Server $server */
        $server = $serverRepo->find($request->get('server'));
        $user = $this->container->get('doctrine.orm.entity_manager')->getRepository(User::class)->find($request->get('user'));

        $serverRepo->setRunningHook($server, $user);

        $process = new Process(sprintf('cap %s server_hook_%s_%s', $server->getId(), $request->get('hook'), $server->getId()));
        $process->setWorkingDirectory($this->container->get('ci_app.structure')->getBasePath($server->getApplication()));
        $process->setTimeout(0);
        $process->start();
        $process->wait(function ($type, $buffer) use ($em, $server, $queue, &$response) {
            $output = $queue->getResponse();
            $output = true === is_array($output) && true === array_key_exists('output', $output) ? $output['output'] : '';

            $queue->setResponse(
                [
                    'status' => (string) $server->getStatusType(),
                    'output' => $output . $buffer
                ]);

            $em->persist($queue);
            $em->flush($queue);
        });

        $this->response->setSuccess(0 === preg_match('/cap aborted/', $queue->getResponse()['output']));
        $this->response->setResponse($queue->getResponse());

        $serverRepo->setHookRan($server, $user);
        $serverRepo->setReady($server, $user);
    }

    public static function createRequest(Server $server, Hook $hook, User $user)
    {
        return [
            'server' => $server->getId(),
            'hook' => $hook->getId(),
            'user' => $user->getId(),
        ];
    }
}
