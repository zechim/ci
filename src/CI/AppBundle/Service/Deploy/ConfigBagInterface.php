<?php

namespace CI\AppBundle\Service\Deploy;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\Build;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Service\Deploy\Structure\StructureInterface;
use Symfony\Component\Templating\EngineInterface;

interface ConfigBagInterface
{
    /**
     * @param Build $build
     * @return string
     */
    public function getBuildPath(Build $build);

    /**
     * @param Application $application
     * @return string
     */
    function getBasePath(Application $application);

    /**
     * @param Application $application
     * @return string
     */
    function getCapfilePath(Application $application);

    /**
     * @param Application $application
     * @return string
     */
    function getDeployPath(Application $application);

    /**
     * @param Application $application
     * @return string
     */
    function getConfigPath(Application $application);

    /**
     * @param Server $server
     * @return string
     */
    function getStagePath(Server $server);

    /**
     * @param Application $application
     * @return string
     */
    function getRakePath(Application $application);

    /**
     * @param Server $server
     * @return string
     */
    function getKeyPath(Server $server);

    /**
     * @param Server $server
     * @return string
     */
    function getUploadPath(Server $server);

    /**
     * @return EngineInterface
     */
    function getTemplating();

    /**
     * @return StructureInterface
     */
    function getStructure();

    /**
     * @return string
     */
    function getCapfileTemplate();

    /**
     * @return string
     */
    function getDeployTemplate();

    /**
     * @return string
     */
    function getStageTemplate();

    /**
     * @return array
     */
    function getRakeFilesTemplate();
}
