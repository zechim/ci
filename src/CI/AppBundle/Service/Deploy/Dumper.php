<?php

namespace CI\AppBundle\Service\Deploy;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Service\Deploy\Structure\Capfile;
use CI\AppBundle\Service\Deploy\Structure\Deploy;
use CI\AppBundle\Service\Deploy\Structure\RakeFile;
use CI\AppBundle\Service\Deploy\Structure\Stage;
use CI\AppBundle\Service\Deploy\Structure\StructureInterface;

class Dumper
{
    /**
     * @var StructureInterface
     */
    protected $structure;

    /**
     * Dumper constructor.
     * @param StructureInterface $structure
     */
    public function __construct(StructureInterface $structure)
    {
        $this->structure = $structure;
    }

    public function dumpServer(Server $server)
    {
        (new Capfile($this->structure))->dump($server);
        (new Deploy($this->structure))->dump($server);
        (new RakeFile($this->structure))->dump($server);
        (new Stage($this->structure))->dump($server);
    }
}
