<?php

namespace CI\AppBundle\Service\Pagination;

use Doctrine\Common\Collections\ArrayCollection;

class Pagination extends ArrayCollection
{
    const TOTAL = 'total';
    const RESULT = 'result';
    const PAGES = 'pages';
    const ROUTE = 'route';
    const PARAMETERS = 'parameters';
    const PAGE_PARAMETER = 'page_parameter';

    public function __construct($total, $result, $pages)
    {
        $this->set(self::TOTAL, $total);
        $this->set(self::RESULT, $result);
        $this->set(self::PAGES, $pages);
        $this->set(self::PARAMETERS, []);
        $this->set(self::PAGE_PARAMETER, 'page');
    }
}
