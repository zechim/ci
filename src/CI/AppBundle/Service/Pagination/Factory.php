<?php

namespace CI\AppBundle\Service\Pagination;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\QueryBuilder;

class Factory
{
    public static function getPagination(QueryBuilder $qb, $page, $limit)
    {
        $total = clone $qb;
        $total = $total->select(sprintf('COUNT(%s.id)', $total->getAllAliases()[0]))->getQuery()->getSingleScalarResult();

        $pages = (int) ceil($total / $limit);

        $page = (int) $page;
        $page = 0 >= $page ? 1 : $page;
        $page = $pages < $page  ? $pages : $page;

        $paginator = new Paginator($qb, $page);

        if (0 < $page) {
            $paginator->getQuery()->setFirstResult($limit * ($page - 1));
            $paginator->getQuery()->setMaxResults($limit);
        }

        return new Pagination($total, $paginator->getQuery()->getResult(), $pages);
    }
}
