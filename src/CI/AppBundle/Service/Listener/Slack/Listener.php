<?php

namespace CI\AppBundle\Service\Listener\Slack;

use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\Type;
use CI\AppBundle\Service\Deploy\Queue\Event\DeployEvent;
use CI\AppBundle\Service\Deploy\Queue\Event\DeployEventListenerInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Listener implements DeployEventListenerInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var array
     */
    protected $parameters;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param array $parameters
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @param DeployEvent $event
     * @return mixed
     */
    public function onDeployEvent(DeployEvent $event)
    {
        $text = true === $event->getResponse()->getSuccess() ? $this->parameters['message']['success'] : $this->parameters['message']['error'];
        $color = true === $event->getResponse()->getSuccess() ? $this->parameters['color']['success'] : $this->parameters['color']['error'];

        /** @var Server $server */
        $server = $event->getResponse()->getResponse()['server'];

        $fields = [];

        $fields[] = [
            "title" => "Application",
            "value" => sprintf(
                '<%s%s|%s>',
                trim($this->parameters['base_url'], '/'),
                $this->container->get('router')->generate(
                    'ci_app_application_show',
                    [
                        'application' => $server->getApplication()->getId()
                    ],
                    Router::ABSOLUTE_PATH
                ),
                (string) $server->getApplication()
            ),
            "short" => true
        ];

        $fields[] = [
            "title" => "Server",
            "value" => sprintf(
                '<%s%s|%s>',
                trim($this->parameters['base_url'], '/'),
                $this->container->get('router')->generate(
                    'ci_app_server_show',
                    [
                        'server' => $server->getId()
                    ],
                    Router::ABSOLUTE_PATH
                ),
                (string) $server
            ),
            "short" => true
        ];

        if (true === $event->getResponse()->getSuccess()) {
            $fields[] = [
                "title" => "Commiter",
                "value" => $this->getCommiterText(
                    $server,
                    $event->getResponse()->getResponse()['gitlog']['commiter']
                ),
                "short" => true
            ];

            $fields[] = [
                "title" => "Commit",
                "value" => $this->getCommitText(
                    $server,
                    $event->getResponse()->getResponse()['gitlog']['commit'],
                    $event->getResponse()->getResponse()['gitlog']['message']
                ),
                "short" => true
            ];

            $fields[] = [
                "title" => "Date",
                "value" => $event->getResponse()->getResponse()['gitlog']['date'],
                "short" => true
            ];
        }

        $data = "payload=".json_encode([
            "pretext" => $this->parameters['pretext'],
            "channel" => sprintf('#%s', trim($this->parameters['channel'], '#')),
            "text" => $text,
            "color" => $color,
            "username" => $this->parameters['username'],
            "fields" => $fields
        ]);

        $ch   = curl_init($this->parameters['endpoint']);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    /**
     * @param Server $server
     * @param $commiter
     * @return string
     */
    protected function getCommiterText(Server $server, $commiter)
    {
        $commiter = trim($commiter);

        if ('' === $commiter) {
            return '';
        }

        $repo = $server->getApplication()->getApplicationConfig(Type::CODE_CONFIG_REPO);

        if (false !== strpos($repo, 'bitbucket')) {
            return sprintf('<https://bitbucket.org/%s|%s>', $commiter, $commiter);
        }

        return $commiter;
    }

    /**
     * @param Server $server
     * @param $commit
     * @param $message
     * @return string
     */
    protected function getCommitText(Server $server, $commit, $message)
    {
        $commit = trim($commit);

        if ('' === $commit) {
            return '';
        }

        $repo = $server->getApplication()->getApplicationConfig(Type::CODE_CONFIG_REPO);

        if (false !== strpos($repo, 'bitbucket')) {
            $repo = str_replace(["git@", ".git"], '', $repo);
            $repo = str_replace(":", '/', $repo);

            $commit = sprintf('<https://%s/commits/%s|%s>', $repo, $commit, $commit);
        }

        return sprintf('%s (%s)', $commit, $message);
    }
}
