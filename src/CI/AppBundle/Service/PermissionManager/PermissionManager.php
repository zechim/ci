<?php

namespace CI\AppBundle\Service\PermissionManager;

use CI\AppBundle\Entity\User;
use CI\AppBundle\Entity\UserAction;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PermissionManager
{
    /**
     * @var \CI\AppBundle\Entity\User
     */
    protected $user;
    
    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface
     */
    protected $tokenStorage;
    
    /**
     * @var \CI\AppBundle\Service\PermissionManager\UserPermissionBag
     */
    protected $permissionBag;

    /**
     * @var $retrieved boolean
     */
    protected $retrieved = false;

    /**
     * PermissionManager constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManagerInterface $em
     */
    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $em)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
    }

    protected function retrieve()
    {
        if (true === $this->retrieved) {
            return;
        }
        
        $user = null;
        
        if (null !== ($token = $this->tokenStorage->getToken())) {
            $user = $token->getUser();
        }

        if (false === $user instanceof User) {
            throw new Exception();
        }

        $this->permissionBag = new UserPermissionBag($user, $this->em);
        $this->user = $user;
        
        $this->retrieved = true;
    }

    /**
     * @param UserAction|string $code
     * @return boolean
     */
    public function hasAction($code)
    {
        $this->retrieve();

        $code = true === $code instanceof UserAction ? $code->getCode() : $code;

        return $this->permissionBag->hasAction($code);
    }

    /**
     * @return \CI\AppBundle\Entity\User
     */
    public function getUser()
    {
        $this->retrieve();

        return $this->user;
    }
}