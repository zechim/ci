<?php

namespace CI\AppBundle\Service\Queue;

use CI\AppBundle\Entity\Queue;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Process\Process;

abstract class AbstractService
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;
    
    /**
     * @var \CI\AppBundle\Service\Queue\Response
     */
    protected $response;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->response = new Response();
    }
    
    abstract public function dispatch(Queue $queue, Request $request);
    
    /**
     * @return \CI\AppBundle\Service\Queue\Response
     */
    final public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param $path
     * @param $command
     * @param \Closure|null $callback
     * @return string
     */
    protected function runCommand($path, $command, \Closure $callback = null)
    {
        $output = '';

        if (null === $callback) {
            $callback = function ($type, $buffer) use (&$output) {
                $output.= $buffer;
            };
        }

        $process = new Process($command);
        $process->setWorkingDirectory($path);
        $process->setTimeout(0);
        $process->start();
        $process->wait($callback);

        return trim($output);
    }
}