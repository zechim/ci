<?php

namespace CI\AppBundle\Service\Queue;

use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Util\Debug;
use CI\AppBundle\Entity\Queue;

class QueueErrorRecovery
{
    /**
     * @var \Doctrine\Bundle\DoctrineBundle\Registry
     */
    public static $doctrine;
    
    /**
     * @var \CI\AppBundle\Entity\Queue
     */
    public static $queue;
    
    /**
     * @var \Symfony\Component\Console\Output\OutputInterface
     */
    public static $output;
    
    protected static function doLog($message)
    {
        if (self::$output instanceof OutputInterface) {
            self::$output->writeln($message);
            
        } else {
            error_log($message);
        }
    }

    /**
     * @param $file
     * @param $line
     * @param $message
     */
    public static function setError($file, $line, $message)
    {
        $message = sprintf(
            'Error on Queue at [%s] on %s:%d with the following message: "%s"',
            date('Y-m-d H:i:s'),
            $file,
            $line,
            str_replace("\n", ' ', $message)
        );

        if (false === self::$doctrine instanceof Registry || false === self::$queue instanceof Queue) {
            self::doLog($message);

            return;
        }

        $em = self::$doctrine->getManager();
        
        if ($em->isOpen() == false) {
            self::$doctrine->resetManager();
            $em = self::$doctrine->getManager();
        }

        $em->getRepository(Queue::class)->setError(
            self::$queue,
            [
                'error' => $message,
                'debug' => Debug::export(debug_backtrace(), 4)
            ]
        );

        self::doLog($message);
    }

    public static function shutDown()
    {
        $error = error_get_last();
        
        if ($error['type'] == E_ERROR) {
            self::setError($error['file'], $error['line'], $error['message']);
        }

        return null;
    }
}