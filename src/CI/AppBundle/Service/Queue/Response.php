<?php

namespace CI\AppBundle\Service\Queue;

class Response
{
    /**
     * @var boolean
     */
    protected $success = true;
    
    /**
     * @var \DateTime
     */
    protected $rescheduleAt;
    
    /**
     * @var array
     */
    protected $response = [];
    
    /**
     * @var boolean
     */
    protected $debug = false;
    
	/**
     * @return boolean $success
     */
    public function getSuccess()
    {
        return $this->success;
    }

	/**
     * @return \DateTime
     */
    public function getRescheduleAt()
    {
        return $this->rescheduleAt;
    }

	/**
     * @param boolean $success
     */
    public function setSuccess($success)
    {
        $this->success = (boolean) $success;
    }

	/**
     * @param \DateTime $rescheduleAt
     */
    public function setRescheduleAt(\DateTime $rescheduleAt)
    {
        $this->rescheduleAt = $rescheduleAt;
    }
    
	/**
     * @return array $message
     */
    public function getResponse()
    {
        return $this->response;
    }

	/**
     * @param array $response
     */
    public function setResponse(array $response)
    {
        $this->response = $response;
    }
    
	/**
     * @return boolean $debug
     */
    public function getDebug()
    {
        return $this->debug;
    }

	/**
     * @param boolean $debug
     */
    public function setDebug($debug)
    {
        $this->debug = (boolean) $debug;
    }
}
