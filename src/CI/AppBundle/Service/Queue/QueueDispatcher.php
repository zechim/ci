<?php

namespace CI\AppBundle\Service\Queue;

use CI\AppBundle\Repository\QueueRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CI\AppBundle\Entity\Queue;

class QueueDispatcher
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * QueueDispatcher constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Queue $queue
     * @return Queue
     */
    public function dispatch(Queue $queue)
    {
        /** @var QueueRepository $queueRepo */
        $queueRepo = $this->getManager()->getRepository('CIAppBundle:Queue');
        $queueRepo->setRunning($queue);
        
        $service = $this->getService($queue->getService());
        $service->dispatch($queue, new Request($queue->getRequest()));

        if (true === $service->getResponse()->getSuccess()) {
            return $queueRepo->setExecuted($queue, $service->getResponse()->getResponse());

        } else {
            return $queueRepo->setError($queue, $service->getResponse()->getResponse());
        }
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected function getManager()
    {
        /** @var Registry $doctrine */
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getManager();
        
        if ($em->isOpen() == false) {
            $doctrine->resetManager();
            $em = $doctrine->getManager();
        }
        
        return $em;
    }

    /**
     * @param $service
     * @return \CI\AppBundle\Service\Queue\AbstractService
     * @throws \Exception
     */
    protected function getService($service)
    {
        $service = str_replace('-', '\\', $service);
        
        if (class_exists($service, true) == false) {
            throw new \Exception(sprintf('Service [%s] not found.', $service));
        }
        
        $class = new $service($this->container);
        
        if ($class instanceof AbstractService === false) {
            throw new \Exception(sprintf('Service [%s] is not a [AbstractService].', $service));
        }
        
        return $class;
    }
}