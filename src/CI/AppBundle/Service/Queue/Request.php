<?php

namespace CI\AppBundle\Service\Queue;

class Request
{
    /**
     * @var array
     */
    protected $request;

    /**
     * Request constructor.
     * @param array $request
     */
    public function __construct(array $request)
    {
        $this->request = $request;
    }

    /**
     * @param $key
     * @param null $default
     * @param bool $ex
     * @return mixed|null
     * @throws \Exception
     */
    public function get($key, $default = null, $ex = false)
    {
        $keyList = explode('.', $key);
        $request = $this->request;
        $found = $default;
        
        foreach ($keyList as $key) {
            if (array_key_exists($key, $request)) {
                $found = $request = $request[$key];
                
                continue;
            }
            
            if (true === $ex) {
                throw new \Exception(sprintf('Item [%s] not found.', implode('.', $keyList)));
            }
            
            $found = $default;
            
            break;
        }
        
        return $found;
    }
}