<?php

namespace CI\AppBundle\Service\Menu;

use Doctrine\ORM\EntityManager;

class MenuCriteria
{
    /**
     * @param EntityManager $em
     * 
     * @return \ArrayObject
     */
	public function build(EntityManager $em)
    {
        /* @var $qb \Doctrine\ORM\QueryBuilder */
        $qb = $em->getRepository('CIAppBundle:Menu')->createQueryBuilder('menu');
        $qb->select('menu, children, parent');
        $qb->leftJoin('menu.children', 'children');
        $qb->leftJoin('menu.parent', 'parent');
        $qb->where('menu.isVisible = 1');

        return new \ArrayObject($qb->getQuery()->getResult());
    }
}