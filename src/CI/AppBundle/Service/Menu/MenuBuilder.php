<?php

namespace CI\AppBundle\Service\Menu;

use CI\AppBundle\Service\PermissionManager\PermissionManager;

use Doctrine\ORM\EntityManager;

class MenuBuilder
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    
    /**
     * @var \CI\AppBundle\Service\PermissionManager\PermissionManager
     */
    protected $pm;

    /**
     * MenuBuilder constructor.
     * @param EntityManager $em
     * @param PermissionManager $pm
     */
    public function __construct(EntityManager $em, PermissionManager $pm)
    {
       $this->em = $em;
       $this->pm = $pm;
    }
   
    /** 
     * @param MenuCriteria $criteria
     * 
     * @return \CI\AppBundle\Service\Menu\MenuRootIterator
     */
    public function build(MenuCriteria $criteria)
    {
        return new MenuRootIterator($criteria->build($this->em), $this->pm);   
    }
}