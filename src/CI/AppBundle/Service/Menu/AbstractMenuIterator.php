<?php

namespace CI\AppBundle\Service\Menu;

use CI\AppBundle\Entity\Menu;
use CI\AppBundle\Service\PermissionManager\PermissionManager;
use CI\AppBundle\Entity\UserAction;

abstract class AbstractMenuIterator implements \Iterator, \Countable
{
    /**
     * @var array
     */
    protected $data;

    protected function checkPermission(Menu $menu, PermissionManager $pm)
    {
        if (true === $menu->getUserAction() instanceof UserAction) {
            return $pm->hasAction($menu->getUserAction()->getCode());
        }
        
        $list = $menu->getChildren()->filter(function(Menu $menu) {
            return true === $menu->getUserAction() instanceof UserAction;
        });

        $total = $list->count();
        
        foreach ($list as $item) {
            if (false === $pm->hasAction($item->getUserAction()->getCode())) {
                $total-= 1;
            }
        }
        
        if (0 === $total) {
            return false;
        }

        return true;
    }
    
    public function next()
    {
        return next($this->data);
    }
    
    public function key()
    {
        return key($this->data);
    }
    
    public function valid()
    {
        return false !== $this->current();
    }
    
    public function rewind()
    {
        return reset($this->data);
    }
    
    public function count()
    {
        return count($this->data);
    }
    
    public function usort(Menu $right, Menu $left)
    {
        if ($right->getPosition() == $left->getPosition()) {
            return 0;
        }
    
        return $right->getPosition() < $left->getPosition() ? -1 : 1;
    }
}