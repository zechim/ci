<?php

namespace CI\AppBundle\Service\Tag\Queue;

use CI\AppBundle\Entity\Build;
use CI\AppBundle\Entity\BuildUpload;
use CI\AppBundle\Entity\Queue;
use CI\AppBundle\Entity\Tag;
use CI\AppBundle\Entity\User;
use CI\AppBundle\Repository\BuildRepository;
use CI\AppBundle\Service\Queue\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

class CreateTagService extends AbstractTagService
{
    public function dispatch(Queue $queue, Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        /** @var BuildRepository $buildRepo */
        $buildRepo = $em->getRepository(Build::class);

        /** @var Build $build */
        $build = $buildRepo->find($request->get('build'));
        $user = $this->container->get('doctrine.orm.entity_manager')->getRepository(User::class)->find($request->get('user'));

        $buildRepo->setBuilding($build, $user);

        $tag = null;
        $commit = null;

        try {
            $this->checkRepository($build);

            $info = $this->getInfo($build);

            if (true === $this->isOutdated($build, $info)) {
                $built = $this->createTag($build, $info);

                $response = $build->getResponse();
                $this->response->setSuccess(1 === preg_match('/(.?)new tag/', $response['output']));
                $this->response->setResponse($response);

                if (true === $this->response->getSuccess()) {
                    $info['tag'] = $built['tag'];
                    $info['commit'] = $built['commit'];
                }

            } else {
                $response = $build->getResponse();
                $response = true === is_array($response) ? $response : [];
                $response['output'] = true === array_key_exists('output', $response) ? $response['output'] : [];
                $response['output'].= sprintf('TAG %s (%s) is up-to-date', $info['tag'], $info['commit']);

                $build->setResponse($response);
                $this->response->setSuccess(true);
            }

            $tag = $info['tag'];
            $commit = $info['commit'];

        } catch (\Exception $e) {
            $response = $build->getResponse();
            $response = true === is_array($response) ? $response : [];
            $response['error'] = sprintf('%s (%s:%s)', $e->getMessage(), $e->getFile(), $e->getLine());

            $build->setResponse($response);
            $this->response->setSuccess(false);
        }

        $buildRepo->setBuilt(
            $build,
            $user,
            $tag,
            $commit,
            $this->response->getSuccess()
        );

        $buildRepo->setReady($build, $user);
    }

    /**
     * Check if:
     * - TAG exists
     * - Commit from current Build.branch is equal to Tag
     * - Files are not modified
     *
     * @param Build $build
     * @param array $info
     * @return bool
     */
    protected function isOutdated(Build $build, array $info)
    {
        $path = $this->getRepoPath($build);

        if (null === $info['tag'] || null == $info['commit']) {
            return true;
        }

        $response = $this->runCommand($path, 'git ls-remote --tags');

        if (0 === preg_match(sprintf('/(.?)%s/', $info['tag']), $response)) {
            return true;
        }

        $this->resetRepoToRemote($build, false);

        if (trim($info['commit']) !== trim($this->getCommit($build))) {
            return true;
        }

        $command = sprintf('git fetch origin %s && ', $info['tag']);
        $command.= sprintf('git checkout %s', $info['tag']);

        $this->runCommand($path, $command);

        $versionedInfo = $path . 'info.ci';

        if (false === is_file($versionedInfo)) {
            return true;
        }

        $versionedInfo = Yaml::parse(file_get_contents($versionedInfo));

        if (trim($info['script']) !== trim($versionedInfo['script'])) {
            return true;
        }

        if (count($info['files']) !== count($versionedInfo['files'])) {
            return true;
        }

        foreach ($info['files'] as $id => $file) {
            if (false === array_key_exists($id, $versionedInfo['files'])) {
                return true;
            }

            if (
                date_create_from_format('YmdHis', $file['updated_at']) >
                date_create_from_format('YmdHis', $versionedInfo['files'][$id]['updated_at'])
            ) {
                return true;
            }
        }

        foreach ($versionedInfo['files'] as $id => $file) {
            if (false === array_key_exists($id, $info['files'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Build $build
     * @return array
     */
    protected function getInfo(Build $build)
    {
        $data = [
            'files' => [],
            'tag' => '',
            'commit' => '',
            'script' => $build->getScript(),
        ];

        /** @var BuildUpload $upload */
        foreach ($build->getBuildUpload() as $upload) {
            $data['files'][$upload->getId()] = [];
            $data['files'][$upload->getId()]['updated_at'] = $upload->getUpdatedAt()->format('YmdHis');

            $filePath = $upload->getTargetPath();
            $filePath = ltrim($filePath, '.');
            $filePath = ltrim($filePath, '/\\');

            $data['files'][$upload->getId()]['path'] = $filePath;
            $data['files'][$upload->getId()]['id'] = $upload->getId();
        }

        $data['tag'] = $build->getCurrentTagName();
        $data['commit'] = $build->getCurrentTagCommit();

        return $data;
    }

    /**
     * @param Build $build
     * @return mixed|null
     */
    public function getCommit(Build $build)
    {
        $command = sprintf("git log %s -1 --pretty=format:'gitlog: %%h'", $build->getBranch());

        preg_match('/(.?)gitlog:[^\r\n]*/m', trim($this->runCommand($this->getRepoPath($build), $command)), $match);

        if (false === is_array($match)) {
            return null;
        }

        $match = reset($match);
        $match = trim(str_replace('gitlog:', '', $match));
        $match = explode(';', $match);

        if (1 !== count($match)) {
            return null;
        }

        return array_shift($match);
    }

    protected function createTag(Build $build, array $info)
    {
        $this->resetRepoToRemote($build);

        $commit = $this->getCommit($build);
        $tag = $this->container->get('doctrine.orm.entity_manager')->getRepository(Tag::class)->createName($build);

        $info['tag'] = $tag;
        $info['commit'] = $commit;

        /** @var BuildUpload $buildUpload */
        foreach ($build->getBuildUpload() as $buildUpload) {
            (new Filesystem())->dumpFile(
                sprintf('%s%s', $this->getRepoPath($build), $buildUpload->getTargetPath()),
                $buildUpload->getValue()
            );
        }

        $path = $this->getRepoPath($build);

        $command = sprintf('cd %s && ', $path);

        $script = trim($build->getScript(), ' \t\n\r;');

        if ('' !== $script) {
            $command.= $script . ' && ';
        }

        file_put_contents(sprintf('%s../info_new.ci', $path), Yaml::dump($info, 10));

        $command.= sprintf('cp %s../info_new.ci %sinfo.ci && git add info.ci && ', $path, $path);
        $command.= sprintf("git commit -m '%s' && ", $tag);
        $command.= sprintf("git tag -a -m 'CI auto building (tagged %s)' '%s' && ", $tag, $tag);
        $command.= sprintf('git push origin %s', $tag);

        $this->runCommand($path, $command, $this->getUpdateBuildCallback($build));

        return ['tag' => $tag, 'commit' => $commit];
    }

    public static function createRequest(Build $build, User $user)
    {
        return [
            'build' => $build->getId(),
            'user' => $user->getId(),
        ];
    }
}
