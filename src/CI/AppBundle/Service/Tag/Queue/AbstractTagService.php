<?php

namespace CI\AppBundle\Service\Tag\Queue;

use CI\AppBundle\Entity\Build;
use CI\AppBundle\Entity\BuildUpload;
use CI\AppBundle\Entity\Queue;
use CI\AppBundle\Entity\Tag;
use CI\AppBundle\Entity\User;
use CI\AppBundle\Repository\BuildRepository;
use CI\AppBundle\Service\Queue\AbstractService;
use CI\AppBundle\Service\Queue\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use Symfony\Component\Yaml\Yaml;

abstract class AbstractTagService extends AbstractService
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @param Build $build
     * @return string
     */
    protected function getBuildPath(Build $build)
    {
        if (null !== $this->path) {
            return $this->path;
        }

        $path = $this->container->get('ci_app.config')->getBuildPath($build);

        if (false === is_dir($path)) {
            mkdir($path, 0777, true);
        }
        
        return $this->path = $path;
    }

    /**
     * @param Build $build
     * @return string
     */
    protected function getRepoPath(Build $build)
    {
        return $this->getBuildPath($build) . 'repo' . DIRECTORY_SEPARATOR;
    }

    /**
     * @param Build $build
     * @param bool $reset
     */
    protected function checkRepository(Build $build, $reset = true)
    {
        $repoPath = $this->getRepoPath($build);

        $isRepository = true === is_dir(sprintf('%s.git%s', $repoPath, DIRECTORY_SEPARATOR));
        $isSameRemote = $build->getRepo() === $this->runCommand($repoPath, sprintf('cd %s && git config --get remote.origin.url', $repoPath));

        $clone = false;

        if (false === $isRepository || false === $isSameRemote) {
            $clone = true;
            $this->runCommand($repoPath, sprintf('rm -rf %s', $repoPath));
        }

        if (true === $clone) {
            $this->runCommand($this->getBuildPath($build), sprintf('git clone %s repo', $build->getRepo()));

        } else {
            $this->runCommand($repoPath, sprintf('cd %s && git reflog expire --expire=now --all && git repack -ad && git prune', $repoPath));

        }

        $response = $this->runCommand($repoPath, sprintf('git fetch origin %s', $build->getBranch()));

        if (1 === preg_match('/(.?)fatal/', $response)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Branch %s does not exists in %s.',
                    $build->getBranch(),
                    $build->getRepo()
                )
            );
        }

        if (true === $reset) {
            $this->resetRepoToRemote($build);
        }
    }

    /**
     * @param Build $build
     * @param bool $saveOutput
     */
    protected function resetRepoToRemote(Build $build, $saveOutput = true)
    {
        $path = $this->getRepoPath($build);

        $head = $this->runCommand($path, sprintf('cd %s && git ls-remote origin %s', $path, $build->getBranch()));
        $head = strtok($head, "\t");

        $command = sprintf('cd %s && ', $path);
        $command.= sprintf('touch %s.tmp && git add . && git stash && git stash drop && ', uniqid('tmp_ci', true));
        $command.= sprintf('git checkout %s --progress && ', $build->getBranch());
        $command.= sprintf('git pull --rebase origin %s && ', $build->getBranch());
        $command.= sprintf('git reset %s --hard', $head);

        $callback = null;

        if (true === $saveOutput) {
            $callback = $this->getUpdateBuildCallback($build);
        }

        $this->runCommand($path, $command, $callback);
    }

    /**
     * @param Tag $tag
     */
    protected function deleteTag(Tag $tag)
    {
        $path = $this->getRepoPath($tag->getBuild());

        $this->runCommand($path, sprintf('cd %s && git tag -d %s', $path, $tag->getName()));
        $this->runCommand($path, sprintf('cd %s && git push origin :refs/tags/%s', $path, $tag->getName()));
    }

    /**
     * @param Build $build
     * @return \Closure
     */
    protected function getUpdateBuildCallback(Build $build)
    {
        return function ($type, $buffer) use ($build) {
            $output = $build->getResponse();
            $output = true === is_array($output) && true === array_key_exists('output', $output) ? $output['output'] : '';

            $build->setResponse(['output' => $output . $buffer]);

            $this->container->get('doctrine.orm.entity_manager')->persist($build);
            $this->container->get('doctrine.orm.entity_manager')->flush($build);
        };
    }
}
