<?php

namespace CI\AppBundle\Service\Tag\Queue;

use CI\AppBundle\Entity\Queue;
use CI\AppBundle\Entity\Tag;
use CI\AppBundle\Entity\User;
use CI\AppBundle\Repository\TagRepository;
use CI\AppBundle\Service\Queue\Request;
use Doctrine\ORM\EntityManager;

class DeleteTagService extends AbstractTagService
{
    public function dispatch(Queue $queue, Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        /** @var TagRepository $tagRepo */
        $tagRepo = $em->getRepository(Tag::class);

        /** @var Tag $tag */
        $tag = $tagRepo->find($request->get('tag'));

        $this->resetRepoToRemote($tag->getBuild());
        $this->deleteTag($tag);

        $tagRepo->delete(
            $tag,
            $this->container->get('doctrine.orm.entity_manager')->getRepository(User::class)->find($request->get('user')),
            false
        );
    }

    public static function createRequest(Tag $tag, User $user)
    {
        return [
            'tag' => $tag->getId(),
            'user' => $user->getId(),
        ];
    }
}
