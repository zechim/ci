<?php

namespace CI\ApiBundle\Service;

use CI\AppBundle\Entity\User;
use CI\AppBundle\Entity\UserToken;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Translation\TranslatorInterface;

class UserProvider implements UserProviderInterface
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    public function __construct(TranslatorInterface $translator, EntityManager $em)
    {
        $this->translator = $translator;
        $this->em = $em;
    }

    /**
     * @param string $username
     * @return User
     */
    public function loadUserByUsername($username)
    {
        return null;
    }

    /**
     * @param $token
     * @return UserToken
     */
    public function loadUserByToken($token)
    {
        $userToken = $this->em->getRepository('CIAppBundle:UserToken')->findOneBy(['token' => $token]);

        if ($userToken == null) {
            throw new BadCredentialsException($this->translator->trans("trans.api.message.token.not.found", ['%token%' => $token]));
        }

        return $userToken;
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        throw new UnsupportedUserException();
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return true;
    }
}
