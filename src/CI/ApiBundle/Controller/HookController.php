<?php

namespace CI\ApiBundle\Controller;

use CI\AppBundle\Entity\Hook;
use CI\AppBundle\Entity\Server;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class HookController extends Controller
{
    public function indexAction()
    {
        return new JsonResponse($this->getDoctrine()->getRepository(Hook::class)->getHookList());
    }

    public function deleteAction(Request $request)
    {
        $hook = $this->getDoctrine()->getRepository(Hook::class)->find((int) $request->get('hook', 0));

        if (null === $hook) {
            return new JsonResponse(['error' => true, 'message' => $this->get('translator')->trans('trans.api.hook.not.found')]);
        }

        return $this->forward('CIAppBundle:Hook:delete', [
            'api' => true,
            'hook' => $hook
        ]);
    }

    public function runAction(Request $request)
    {
        $hook = $this->getDoctrine()->getRepository(Hook::class)->find((int) $request->get('hook', 0));

        if (null === $hook) {
            return new JsonResponse(['error' => true, 'message' => $this->get('translator')->trans('trans.api.hook.not.found')]);
        }

        $server = $this->getDoctrine()->getRepository(Server::class)->find((int) $request->get('server', 0));

        if (null === $server) {
            return new JsonResponse(['error' => true, 'message' => $this->get('translator')->trans('trans.api.server.not.found')]);
        }

        return $this->forward('CIAppBundle:Hook:run', [
            'api' => true,
            'server' => $server,
            'hook' => $hook
        ]);
    }
}
