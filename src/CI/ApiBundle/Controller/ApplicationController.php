<?php

namespace CI\ApiBundle\Controller;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\ApplicationConfig;
use CI\AppBundle\Entity\Type;
use CI\AppBundle\Form\ApplicationType;
use CI\AppBundle\Repository\ApplicationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApplicationController extends Controller
{
    public function indexAction()
    {
        $response = [];

        /** @var Application $application */
        foreach ($this->getDoctrine()->getRepository(Application::class)->findAll() as $application) {
            $response[$application->getId()] = [
                'id' => $application->getId(),
                'name' => (string) $application,
                'repo' => $application->getApplicationConfig(Type::CODE_CONFIG_REPO),
            ];
        }

        return new JsonResponse($response);
    }

    public function createAction(Request $request)
    {
        $request->request->set('ci_appbundle_application', $request->request->all());

        return $this->forward('CIAppBundle:Application:create', [
            'ci_appbundle_application' => $request->request->all(),
            'api' => true
        ]);
    }

    public function editAction(Request $request)
    {
        $application = $this->getDoctrine()->getRepository(Application::class)->find((int) $request->get('application', 0));

        if (null === $application) {
            return new JsonResponse(['error' => true, 'message' => $this->get('translator')->trans('trans.api.application.not.found')]);
        }

        $data = $request->request->all();
        unset($data['application']);

        $request->request->set('ci_appbundle_application', $data);

        return $this->forward('CIAppBundle:Application:edit', [
            'ci_appbundle_application' => $request->request->all(),
            'api' => true,
            'application' => $application
        ]);
    }

    public function deleteAction(Request $request)
    {
        $application = $this->getDoctrine()->getRepository(Application::class)->find((int) $request->get('application', 0));

        if (null === $application) {
            return new JsonResponse(['error' => true, 'message' => $this->get('translator')->trans('trans.api.application.not.found')]);
        }

        $request->request->set('ci_appbundle_application', $request->request->all());

        return $this->forward('CIAppBundle:Application:delete', [
            'api' => true,
            'application' => $application
        ]);
    }

    public function formExplainAction()
    {
        /** @var ApplicationRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Application::class);

        $form = $this->createForm(
            ApplicationType::class,
            $repo->getDefault(),
            [
                'applicationConfigRepo' => $this->getDoctrine()->getRepository(ApplicationConfig::class),
                'translator' => $this->get('translator')
            ]
        );

        return new JsonResponse($this->get('ci.form.explorer')->explain($form));
    }

    public function deployAction(Request $request)
    {
        $application = $this->getDoctrine()->getRepository(Application::class)->find((int) $request->get('application', 0));

        if (null === $application) {
            return new JsonResponse(['error' => true, 'message' => $this->get('translator')->trans('trans.api.application.not.found')]);
        }

        return $this->forward('CIAppBundle:Application:deploy', [
            'api' => true,
            'application' => $application
        ]);
    }
}
