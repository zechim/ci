<?php

namespace CI\ApiBundle\Controller;

use CI\AppBundle\Entity\Application;
use CI\AppBundle\Entity\History;
use CI\AppBundle\Entity\Server;
use CI\AppBundle\Entity\ServerConfig;
use CI\AppBundle\Entity\Type;
use CI\AppBundle\Form\ServerType;
use CI\AppBundle\Repository\ServerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ServerController extends Controller
{
    public function createAction(Request $request)
    {
        $request->request->set('ci_appbundle_server', $request->request->all());

        return $this->forward('CIAppBundle:Server:create', [
            'ci_appbundle_server' => $request->request->all(),
            'api' => true
        ]);
    }

    public function editAction(Request $request)
    {
        $server = $this->getDoctrine()->getRepository(Server::class)->find((int) $request->get('server', 0));

        if (null === $server) {
            return new JsonResponse(['error' => true, 'message' => $this->get('translator')->trans('trans.api.server.not.found')]);
        }

        $data = $request->request->all();
        unset($data['server']);

        $request->request->set('ci_appbundle_server', $data);

        return $this->forward('CIAppBundle:Server:edit', [
            'ci_appbundle_server' => $request->request->all(),
            'api' => true,
            'server' => $server
        ]);
    }

    public function showAction(Request $request)
    {
        /** @var Server $server */
        $server = $this->getDoctrine()->getRepository(Server::class)->find((int) $request->get('server', 0));

        if (null === $server) {
            return new JsonResponse(['error' => true, 'message' => $this->get('translator')->trans('trans.api.server.not.found')]);
        }

        $response = [];
        $response['name'] = $server->getName();
        $response['application'] = [
            'id' => $server->getApplication()->getId(),
            'name' => (string) $server->getApplication(),
            'status' => (string) $server->getApplication()->getStatusType()
        ];
        $response['status'] =  (string) $server->getStatusType();
        $response['ip'] = $server->getServerConfig(Type::CODE_CONFIG_IP);

        $response['hook'] = [];

        foreach ($server->getServerHook(true) as $hook) {
            $response['hook'][$hook->getHook()->getId()] = (string) $hook;
        }

        $response['history'] = [];

        $i = 0;

        /** @var History $history */
        foreach ($server->getHistory() as $history) {
            if (5 === $i) {
                break;
            }

            $executedAt = $history->getQueue()->getExecutedAt();
            $executedAt = $executedAt instanceof \DateTime ? $executedAt->format('Y-m-d H:i:s') : null;

            $response['history'][] = [
                'id' => $history->getId(),
                'queue' => $history->getQueue()->getId(),
                'response' => json_encode($history->getQueue()->getResponse()),
                'createdAt' => $history->getQueue()->getCreatedAt()->format('Y-m-d H:i:s'),
                'scheduledAt' => $history->getQueue()->getScheduledAt()->format('Y-m-d H:i:s'),
                'status' => (string) $history->getQueue()->getStatusType(),
                'executedAt' => $executedAt
            ];

            $i++;
        }


        return new JsonResponse($response);
    }

    public function deleteAction(Request $request)
    {
        $server = $this->getDoctrine()->getRepository(Server::class)->find((int) $request->get('server', 0));

        if (null === $server) {
            return new JsonResponse(['error' => true, 'message' => $this->get('translator')->trans('trans.api.server.not.found')]);
        }

        $request->request->set('ci_appbundle_server', $request->request->all());

        return $this->forward('CIAppBundle:Server:delete', [
            'api' => true,
            'server' => $server
        ]);
    }

    public function formExplainAction(Request $request)
    {
        $application = $this->getDoctrine()->getRepository(Application::class)->find((int) $request->get('application', 0));

        if (null === $application) {
            return new JsonResponse(['error' => true, 'message' => $this->get('translator')->trans('trans.api.application.not.found')]);
        }

        /** @var ServerRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Server::class);

        $form = $this->createForm(
            ServerType::class,
            $repo->getDefault($application),
            [
                'serverConfigRepo' => $this->getDoctrine()->getRepository(ServerConfig::class),
                'translator' => $this->get('translator'),
            ]
        );

        return new JsonResponse($this->get('ci.form.explorer')->explain($form));
    }

    public function deployAction(Request $request)
    {
        $server = $this->getDoctrine()->getRepository(Server::class)->find((int) $request->get('server', 0));

        if (null === $server) {
            return new JsonResponse(['error' => true, 'message' => $this->get('translator')->trans('trans.api.server.not.found')]);
        }

        return $this->forward('CIAppBundle:Server:deploy', [
            'api' => true,
            'server' => $server
        ]);
    }
}
