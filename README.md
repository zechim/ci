## Ruby (Ubuntu)
`sudo apt-get install ruby2.3 ruby2.3-dev`

## MySQL (Gem)
`apt-get install build-essential bison openssl libreadline6 libreadline6-dev curl git-core zlib1g zlib1g-dev libssl-dev libyaml-dev libxml2-dev autoconf libc6-dev ncurses-dev automake libtool`

`apt-get install libmysqlclient-dev`

`gem install mysql2`

## Capistrano
`gem install capistrano`

## PHP Dependecy
`apt-get install php7.0-curl php7.0-xml php7.0-zip`

## Setup
On application root:

1 `composer install`

2 `bin/console doctrine:database:create`

3 `bin/console doctrine:schema:create`

4 `bin/console doctrine:migrations:migrate -n`

## Update
On application root:

1 `git pull --rebase origin master`

2 `composer update`

3 `bin/console doctrine:schema:update --force`

4 `bin/console d:m:m -n`

## Extra

Generate a private key

on ~/.ssh

1  `ssh-keygen -t rsa -b 2048 -v`

2 `cat id_rsa.pub >> authorized_keys`

3 `chmod 600 authorized_keys`

4 `chown CURRENT_USER *`

Download/copy id_rsa (private key) to the server/application config