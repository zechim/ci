#!/bin/bash

dispatcher=$(cd -P -- "$(dirname -- "$0")" && pwd -P)
dispatcher="$dispatcher/queue.rb"

if ps ax | grep -v grep | grep queue.rb > /dev/null
then
    exit
else
    `ruby $dispatcher >> /var/log/queue.log 2>&1 &` 
fi