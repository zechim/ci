require 'mysql2'
require 'yaml'

def connect(config)
  return Mysql2::Client.new :host => config['parameters']['database_host'], :username => config['parameters']['database_user'], :password => config['parameters']['database_password'], :database => config['parameters']['database_name']
end

path = File.dirname(__FILE__) + '/'
config = YAML.load(File.read(path + '../app/config/parameters.yml'))
db = connect(config)

puts("Starting Queue Dispatcher v1.0")

killed = false

Signal.trap("SIGTERM") { 
    killed = true
    puts("Exiting Queue Dispatcher v1.0")
}

until killed === true  do
  begin
    db.query("SELECT q.id
    			FROM queue q
    			JOIN type t ON t.id = q.status_type_id
    		   WHERE t.code = 'QUEUE_STATUS_WAITING' AND q.scheduled_at <= NOW()
    		ORDER BY q.scheduled_at ASC").each do |result|
      Thread.new {
	 	r = `#{path}console ci:queue --q '#{result['id']}' -vvv`
	 	r.split("\n").each do|c|
            puts(c)
        end
      }
    end
	rescue Exception => e
      puts(e.message)
      db = connect(config)
  end
  sleep(1)
end
